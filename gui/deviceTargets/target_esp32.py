import deviceTargets.gpio as gpio

deviceName = "ESP32"

usb_hid = False
bluetooth_hid = True

gpios = []
gpios.append(gpio.Pin(0, gpio.NO, gpio.NO, "Flashing Enable Pin, must be LOW to enter flashing mode"))
gpios.append(gpio.Pin(1, gpio.NO, gpio.NO, "Debug output"))
gpios.append(gpio.Pin(2, gpio.YES, gpio.YES, "Must be floating or LOW to enter flashing mode"))
gpios.append(gpio.Pin(3, gpio.NO, gpio.NO, "HIGH at boot"))
gpios.append(gpio.Pin(4, gpio.YES, gpio.YES))
gpios.append(gpio.Pin(5, gpio.YES, gpio.YES, "Outputs PWM signal at boot, must be HIGH during boot"))
gpios.append(gpio.Pin(6, gpio.NO, gpio.NO, "Connected to integrated SPI flash"))
gpios.append(gpio.Pin(7, gpio.NO, gpio.NO, "Connected to integrated SPI flash"))
gpios.append(gpio.Pin(8, gpio.NO, gpio.NO, "Connected to integrated SPI flash"))
gpios.append(gpio.Pin(9, gpio.NO, gpio.NO, "Connected to integrated SPI flash"))
gpios.append(gpio.Pin(10, gpio.NO, gpio.NO, "Connected to integrated SPI flash"))
gpios.append(gpio.Pin(11, gpio.NO, gpio.NO, "Connected to integrated SPI flash"))
gpios.append(gpio.Pin(12, gpio.YES, gpio.YES, "Must be LOW during boot"))
gpios.append(gpio.Pin(13, gpio.YES, gpio.YES))
gpios.append(gpio.Pin(14, gpio.YES, gpio.YES, "Outputs PWM signal at boot"))
gpios.append(gpio.Pin(15, gpio.YES, gpio.YES, "Outputs PWM signal at boot, must be HIGH during boot"))
gpios.append(gpio.Pin(16, gpio.YES, gpio.YES))
gpios.append(gpio.Pin(17, gpio.YES, gpio.YES))
gpios.append(gpio.Pin(18, gpio.YES, gpio.YES))
gpios.append(gpio.Pin(19, gpio.YES, gpio.YES))
gpios.append(gpio.Pin(21, gpio.YES, gpio.YES))
gpios.append(gpio.Pin(22, gpio.YES, gpio.YES))
gpios.append(gpio.Pin(23, gpio.YES, gpio.YES))
gpios.append(gpio.Pin(25, gpio.YES, gpio.YES))
gpios.append(gpio.Pin(26, gpio.YES, gpio.YES))
gpios.append(gpio.Pin(27, gpio.YES, gpio.YES))
gpios.append(gpio.Pin(32, gpio.YES, gpio.YES))
gpios.append(gpio.Pin(33, gpio.YES, gpio.YES))
gpios.append(gpio.Pin(34, gpio.YES, gpio.NO, "Input only"))
gpios.append(gpio.Pin(35, gpio.YES, gpio.NO, "Input only"))
gpios.append(gpio.Pin(36, gpio.YES, gpio.NO, "Input only"))
gpios.append(gpio.Pin(39, gpio.YES, gpio.NO, "Input only"))


