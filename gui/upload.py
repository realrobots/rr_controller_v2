# esptool interface
import esptool

def erase_flash(port):
    command = []
    command.append("--port")
    command.append(port)
    command.extend(["--baud", "115200",
                    "--after", "no_reset",
                    "erase_flash"])
    print("Command: esptool.py %s/n" % " ".join(command))

    esptool.main(command)


def merge_firmware():
    command = []
    command.extend(["--chip", "esp32",
                    "merge_bin", "-o", "merged-firmware.bin",
                    "--flash_mode", "dio",
                    "--flash_freq", "40m",
                    "--flash_size", "4MB",
                    "0x1000", "../output/rr_controller_v2.ino.bootloader.bin",
                    "0x8000", '../output/rr_controller_v2.ino.partitions.bin',
                    "0xe000", "./deviceTargets/esp32_boot_app0.bin",
                    "0x10000", "../output/rr_controller_v2.ino.bin"])

    esptool.main(command)



def flash_firmware(port):

    command = []
    command.extend(["--chip", "esp32",
                    "--port", port,
                    "--baud", "921611",
                    "--before", "default_reset",
                    "--after", "hard_reset",
                    "write_flash", "-z",
                    "--flash_mode", "dio",
                    "--flash_size", "detect",
                    "0x1000", "../output/rr_controller_v2.ino.bootloader.bin",
                    "0x8000", '../output/rr_controller_v2.ino.partitions.bin',
                    "0xe000", "./deviceTargets/esp32_boot_app0.bin",
                    "0x10000", "../output/rr_controller_v2.ino.bin"])

    # if self._config.erase_before_flash:
    #     command.append("--erase-all")

    # print("Command: esptool.py %s/n" % " ".join(line))

    esptool.main(command)


# erase_flash()

#flash_firmware()
#print("Firmware uploaded")