import constant
from PySide2.QtCore import QByteArray


class Device():
    def __init__(self):
        self.deviceName = "unknown device"
        self.firmwareVersion = 0
        self.microcontroller = 0
        
        self.deviceBlueprint = None        
        self.inputs = []
        self.inputCount = 0
        self.inputsLocked = True

    def SetFromConfigPacket(self, data):
        self.microcontroller = data[1]
        self.deviceBlueprint = constant.GetDeviceBlueprint(self.microcontroller)
        self.firmwareVersion = data[2]
        self.inputCount = data[3]

        print("Initialised Device " + self.deviceBlueprint.deviceName)
        print("firmwareVersion: " + str(self.firmwareVersion))
        print("input count: " + str(self.inputCount))

    def AddInput(self, data):
        #print("adding input")
        input = DeviceInput()
        input.SetConfig(data)
        self.inputs.append(input)

    def AddInputDefault(self):
        print("adding default input")
        input = DeviceInput()
        self.inputs.append(input)

    def DeleteInput(self, index):
        self.inputs.pop(index)

    def SetPin(self, inputIdx, newPin):
        if not self.inputsLocked:
            self.inputs[inputIdx].SetPin(newPin)

    def SetPinMode(self, inputIdx, newPinMode):
        if not self.inputsLocked:
            self.inputs[inputIdx].SetPinMode(newPinMode)

    def SetIsAnalog(self, inputIdx, isAnalog):
        if not self.inputsLocked:
            self.inputs[inputIdx].SetIsAnalog(isAnalog)

    def SetIsInverted(self, inputIdx, isInverted):
        if not self.inputsLocked:
            self.inputs[inputIdx].SetIsInverted(isInverted)

    def SetMinVal(self, inputIdx, val):
        if not self.inputsLocked:
            self.inputs[inputIdx].SetMinVal(val)

    def SetMidVal(self, inputIdx, val):
        if not self.inputsLocked:
            self.inputs[inputIdx].SetMidVal(val)

    def SetMaxVal(self, inputIdx, val):
        if not self.inputsLocked:
            self.inputs[inputIdx].SetMaxVal(val)

    def SetDeadZone(self, inputIdx, val):
        if not self.inputsLocked:
            self.inputs[inputIdx].SetDeadZone(val)

    def SetFilterSize(self, inputIdx, val):
        if not self.inputsLocked:
            self.inputs[inputIdx].SetFilterSize(val)

    def SetBinding(self, inputIdx, inputType, assignedInput):
        if not self.inputsLocked:
            self.inputs[inputIdx].SetBinding(inputType, assignedInput)    

    def SetInputLock(self, isLocked):
        self.inputsLocked = isLocked
            
    def ToBytes(self):
        bytes = bytearray()
        bytes.append(self.microcontroller)
        bytes.append(self.firmwareVersion)
        bytes.append(self.inputCount)

        count = 0
        for input in self.inputs:
            bytes.append(count)
            count += 1
            for b in input.ToBytes():
                bytes.append(b)

        return bytes



class DeviceInput():
    def __init__(self):
        self.pin = 0
        self.pinMode = constant.IndexToPinMode(0)
        self.isAnalog = 0
        self.isInverted = 0
        self.minVal = 0
        self.midVal = 2048
        self.maxVal = 4096
        self.deadZone = 512
        self.enableFiltering = 0
        self.bufferSize = 0

        self.binding = Binding()

    def SetPin(self, newPin):
        self.pin = newPin

    def SetPinMode(self, newMode):
        self.pinMode = newMode

    def SetIsAnalog(self, isAnalog):
        self.isAnalog = isAnalog

    def SetIsInverted(self, isInverted):
        self.isInverted = isInverted
        #print(self.isInverted)

    def SetMinVal(self, val):
        self.minVal = val

    def SetMidVal(self, val):
        self.midVal = val

    def SetMaxVal(self, val):
        self.maxVal = val

    def SetDeadZone(self, val):
        self.deadZone = val    

    def SetFilterSize(self, val):
        self.bufferSize = val

    def SetBinding(self, inputType, assignedInput):
        self.binding.inputType = inputType
        self.binding.assignedInput = assignedInput

    def SetConfig(self, data):
        self.pin = data[0]
        self.pinMode = data[1]
        self.isAnalog = data[2]
        self.isInverted = data[3]
        self.minVal = data[4] << 8
        self.minVal += data[5]
        self.midVal = data[6] << 8
        self.midVal += data[7]
        self.maxVal = data[8] << 8
        self.maxVal += data[9]
        self.deadZone = data[10] << 8
        self.deadZone += data[11]
        self.bufferSize = data[12]

        self.binding.SetConfig(data)

    def ToBytes(self):
        bytes = bytearray()
        bytes.append(self.pin)
        bytes.append(self.pinMode)
        bytes.append(self.isAnalog)
        bytes.append(self.isInverted)
        c, f = constant.SplitBytes(self.minVal)
        bytes.append(c)
        bytes.append(f)
        c, f = constant.SplitBytes(self.midVal)
        bytes.append(c)
        bytes.append(f)
        c, f = constant.SplitBytes(self.maxVal)
        bytes.append(c)
        bytes.append(f)
        c, f = constant.SplitBytes(self.deadZone)
        bytes.append(c)
        bytes.append(f)
        bytes.append(self.bufferSize)

        for b in self.binding.ToBytes():
            bytes.append(b)

        return bytes

        
class Binding():
    def __init__(self):
        self.deviceType = constant.GAMEPAD
        self.inputType = constant.GAMEPAD_BUTTON
        self.assignedInput = 0
        self.value = 0
        self.state = constant.BUTTON_UP
        self.trigger = 4

    def SetConfig(self, data):
        self.deviceType = data[13]
        self.inputType = data[14]
        self.assignedInput = data[15]
        self.value = data[16] << 8
        self.value += data[17]        
        self.state = data[18]
        self.trigger = data[19]

    def ToBytes(self):
        bytes = bytearray()
        bytes.append(self.deviceType)
        bytes.append(self.inputType)
        bytes.append(self.assignedInput)
        bytes.append(self.state)
        bytes.append(self.trigger)
        return bytes

