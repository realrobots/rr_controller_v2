import ser
import constant
import upload
from functools import partial
from PySide2.QtWidgets import *
from PySide2.QtCore import Qt

class InputListPage():
    def __init__(self, master):
        self.master = master
        self.container = master.gridInputList
        self.master.buttonAddInput.clicked.connect(self.OnButtonAddInput)
    
        self.inputRow = self.GetMasterInputRow()

        self.inputRows = []

        self.RemoveMasterRow()

        # Input List is now empty, AddInputRow() for each of the devices rows
    
    def UpdateRowFromDeviceInput(self, inputIdx):
        i = inputIdx
        device = self.master.device
        row = self.inputRows[i]

        # Populate comboPin choices
        row["comboPin"].clear()
        for item in device.deviceBlueprint.gpios:
            if item.input == 1:
                row["comboPin"].addItem(str(item.gpio))
        
        # Set comboPin choice
        row["comboPin"].setCurrentText(str(device.inputs[i].pin))


        # Populate comboPinMode choices
        row["comboPinMode"].clear()            
        for pinMode in constant.pinModes:
            row["comboPinMode"].addItem(pinMode)
        
        # Set comboPinMode choice
        idx = constant.PinModeToIndex(device.inputs[i].pinMode)
        
        row["comboPinMode"].setCurrentText(constant.pinModes[idx])

        # Populate comboAnalogDigital choices
        row["comboAnalogDigital"].clear()
        for mode in constant.analogModes:
            row["comboAnalogDigital"].addItem(mode)

        # Set comboAnalogDigital choice
        idx = device.inputs[i].isAnalog
        row["comboAnalogDigital"].setCurrentText(constant.analogModes[idx])
        
        row["checkBoxInverted"].setChecked(device.inputs[i].isInverted)
                    
        row["spinMin"].setValue(device.inputs[i].minVal)
        row["spinCenter"].setValue(device.inputs[i].midVal)
        row["spinMax"].setValue(device.inputs[i].maxVal)
        row["spinDeadZone"].setValue(device.inputs[i].deadZone)
        row["spinFilterSize"].setValue(device.inputs[i].bufferSize)

        row["comboBindingType"].clear()
        for binding in constant.inputTypes:
            row["comboBindingType"].addItem(binding)

        idx = device.inputs[i].binding.inputType
        row["comboBindingType"].setCurrentText(constant.inputTypes[idx])

        # row["comboBinding"].clear()
        # for binding in constant.inputsGamepadCompiled:
        #     row["comboBinding"].addItem(binding)
        
        idx = constant.GetAssignedInput(device.inputs[i].binding)
        row["comboBinding"].setCurrentText(idx)

    def UpdateInputsFromDevice(self):
        device = self.master.device

        for i in device.inputs:
            self.AddInputRow()

        #QComboBox().setCurrentText

        for i in range(len(device.inputs)):
            self.UpdateRowFromDeviceInput(i)

        #self.master.device.SetInputLock(False)

    def disable_inputs(self, state=True):
        pass

    def OnPinChange(self, index, newVal="-1"):
        if not self.master.device.inputsLocked:
            if newVal is None or newVal == "":
                return
            self.master.device.SetPin(index, int(newVal))

            self.master.conn.sendDeviceUpdate(index)

    def OnPinModeChange(self, index, newVal="0"):
        if not self.master.device.inputsLocked:
            if newVal is None or newVal == "":
                return
            if newVal is None or newVal == "":
                return
            widget = self.inputRows[index]["comboPinMode"]
            idx = widget.currentIndex()
            newPinMode = constant.IndexToPinMode(int(idx))
            self.master.device.SetPinMode(index, newPinMode)
            
            self.master.conn.sendDeviceUpdate(index)

    def OnAnalogDigitalChange(self, index, newVal="NONE"):
        if not self.master.device.inputsLocked:
            if newVal is None or newVal == "":
                return
            widget = self.inputRows[index]["comboAnalogDigital"]
            idx = widget.currentIndex()
            self.master.device.SetIsAnalog(index, idx)

            self.master.conn.sendDeviceUpdate(index)
    
    def OnInvertedChange(self, index, newVal="-1"):
        if not self.master.device.inputsLocked:
            if newVal is None or newVal == "":
                return
            if newVal == 2:  # not sure why checked == 2? change to 1
                newVal = 1
            self.master.device.SetIsInverted(index, int(newVal))
            
            self.master.conn.sendDeviceUpdate(index)

    def OnMinValChange(self, index, newVal=0):
        if not self.master.device.inputsLocked:
            self.master.device.SetMinVal(index, newVal)
            
            self.master.conn.sendDeviceUpdate(index)

    def OnMidValChange(self, index, newVal=0):
        if not self.master.device.inputsLocked:
            self.master.device.SetMidVal(index, newVal)
            
            self.master.conn.sendDeviceUpdate(index)

    def OnMaxValChange(self, index, newVal=0):
        if not self.master.device.inputsLocked:
            self.master.device.SetMaxVal(index, newVal)
            
            self.master.conn.sendDeviceUpdate(index)

    def OnDeadZoneChange(self, index, newVal=0):
        if not self.master.device.inputsLocked:
            self.master.device.SetDeadZone(index, newVal)
            
            self.master.conn.sendDeviceUpdate(index)

    def OnFilterSizeChange(self, index, newVal=0):
        if not self.master.device.inputsLocked:
            self.master.device.SetFilterSize(index, newVal)
            
            self.master.conn.sendDeviceUpdate(index)

    def OnBindingTypeChanged(self, index, newVal="NONE", sendUpdate=False):
        if newVal == "" or newVal == None:
            return
        widget = self.inputRows[index]["comboBinding"]
        widget.clear()
        for binding in constant.GetAssignmentList(newVal):
            widget.addItem(binding)

        tgt = 0
        widgetBindingTypeIdx = self.inputRows[index]["comboBindingType"].currentIndex()
        # if widgetBindingTypeIdx == constant.GAMEPAD_BUTTON or widgetBindingTypeIdx == constant.GAMEPAD_HAT:
        #     tgt = tgt + 77
        idx = constant.GetAssignmentList(newVal)[tgt]
        widget.setCurrentIndex(tgt)

        #widget.setCurrentIndex(1)
        #self.OnBindingChanged(index, constant.GetAssignmentList(newVal)[1], True)
        #self.master.device.SetBinding()
            
        if sendUpdate:
            self.master.conn.sendDeviceUpdate(index)

    def OnBindingChanged(self, index, newVal="0", sendUpdate=True):
        if not self.master.device.inputsLocked:
            widgetAssignedInput = self.inputRows[index]["comboBinding"]
            widgetBindingType = self.inputRows[index]["comboBindingType"]

            assignedInputIdx = widgetAssignedInput.currentIndex()
            widgetBindingTypeIdx = widgetBindingType.currentIndex()
            #newPinMode = constant.IndexToPinMode(int(idx))
            # if widgetBindingTypeIdx == constant.GAMEPAD_BUTTON or widgetBindingTypeIdx == constant.GAMEPAD_HAT:
            #     assignedInputIdx = assignedInputIdx + 1
                #print("PLUS 1")
            print(index, widgetBindingTypeIdx, assignedInputIdx)
            self.master.device.SetBinding(index,
                                          widgetBindingTypeIdx,
                                          assignedInputIdx)
            print("A")
            print(widgetBindingTypeIdx)
            print(assignedInputIdx)
            
            if sendUpdate:
                self.master.conn.sendDeviceUpdate(index)

    def UpdateValues(self, index, rawVal, val):
        widgetRaw = self.inputRows[index]["labelRawValue"]
        widgetCalibrated = self.inputRows[index]["progressBarValue"]
        #print(index, rawVal, val)
        widgetRaw.setText(str(rawVal))
        widgetCalibrated.setValue(val)
        

    def RemoveMasterRow(self):
        row = self.GetMasterInputRow()

        for widget in row:
            self.container.removeWidget(row[widget])
            #row[widget].deleteLater() 
            row[widget].hide()

    def RemoveInputRow(self, index):
        targetRow = self.inputRows[index]

        for widget in targetRow:
            self.container.removeWidget(targetRow[widget])
            targetRow[widget].deleteLater()

        self.inputRows.remove(targetRow)
        
        self.UpdateRowPositions()
        self.master.device.DeleteInput(index)
        self.master.conn.sendDeleteInput(index)

    def UpdateRowPositions(self):
        y = 1
        for row in self.inputRows:
            self.container.removeWidget(row["comboPin"])
            self.container.addWidget(row["comboPin"], y, 0, 1, 1)            
            self.container.removeWidget(row["comboPinMode"])
            self.container.addWidget(row["comboPinMode"], y, 1, 1, 1)
            self.container.removeWidget(row["comboAnalogDigital"])
            self.container.addWidget(row["comboAnalogDigital"], y, 2, 1, 1)
            self.container.removeWidget(row["checkBoxInverted"])
            self.container.addWidget(row["checkBoxInverted"], y, 3, 1, 1)
            self.container.removeWidget(row["spinMin"])
            self.container.addWidget(row["spinMin"], y, 4, 1, 1)
            self.container.removeWidget(row["spinCenter"])
            self.container.addWidget(row["spinCenter"], y, 5, 1, 1)
            self.container.removeWidget(row["spinMax"])
            self.container.addWidget(row["spinMax"], y, 6, 1, 1)
            self.container.removeWidget(row["spinDeadZone"])
            self.container.addWidget(row["spinDeadZone"], y, 7, 1, 1)
            self.container.removeWidget(row["spinFilterSize"])
            self.container.addWidget(row["spinFilterSize"], y, 8, 1, 1)
            self.container.removeWidget(row["comboBindingType"])
            self.container.addWidget(row["comboBindingType"], y, 9, 1, 1)
            self.container.removeWidget(row["comboBinding"])
            self.container.addWidget(row["comboBinding"], y, 10, 1, 1)
            self.container.removeWidget(row["labelRawValue"])
            self.container.addWidget(row["labelRawValue"], y, 11, 1, 1)
            self.container.removeWidget(row["progressBarValue"])
            self.container.addWidget(row["progressBarValue"], y, 12, 1, 1)
            self.container.removeWidget(row["buttonDelInputRow"])
            self.container.addWidget(row["buttonDelInputRow"], y, 13, 1, 1)


            # Reconnect to set to correct index after one is deleted
            row["comboPin"].currentTextChanged.disconnect()
            row["comboPin"].currentTextChanged.connect(partial(self.OnPinChange, y-1))

            row["comboPinMode"].currentTextChanged.disconnect()
            row["comboPinMode"].currentTextChanged.connect(partial(self.OnPinModeChange, y-1))

            row["comboAnalogDigital"].currentTextChanged.disconnect()
            row["comboAnalogDigital"].currentTextChanged.connect(partial(self.OnAnalogDigitalChange, y-1))

            row["checkBoxInverted"].stateChanged.disconnect()
            row["checkBoxInverted"].stateChanged.connect(partial(self.OnInvertedChange, y-1))

            row["spinMin"].valueChanged.disconnect()
            row["spinMin"].valueChanged.connect(partial(self.OnMinValChange, y-1))

            row["spinCenter"].valueChanged.disconnect()
            row["spinCenter"].valueChanged.connect(partial(self.OnMidValChange, y-1))

            row["spinMax"].valueChanged.disconnect()
            row["spinMax"].valueChanged.connect(partial(self.OnMaxValChange, y-1))

            row["spinDeadZone"].valueChanged.disconnect()
            row["spinDeadZone"].valueChanged.connect(partial(self.OnDeadZoneChange, y-1))

            row["spinFilterSize"].valueChanged.disconnect()
            row["spinFilterSize"].valueChanged.connect(partial(self.OnFilterSizeChange, y-1))


            row["comboBindingType"].currentTextChanged.disconnect()
            row["comboBindingType"].currentTextChanged.connect(partial(self.OnBindingTypeChanged, y-1))

            row["comboBinding"].currentTextChanged.disconnect()
            row["comboBinding"].currentTextChanged.connect(partial(self.OnBindingChanged, y-1))

            row["buttonDelInputRow"].clicked.disconnect()
            row["buttonDelInputRow"].clicked.connect(partial(self.RemoveInputRow, y-1))

            y = y + 1

        self.buttonAddInput = self.master.buttonAddInput
        self.container.removeWidget(self.buttonAddInput)
        self.container.addWidget(self.buttonAddInput, y+1, 0, 1, 1)

        self.container.removeItem(self.master.verticalSpacer)
        self.container.addItem(self.master.verticalSpacer, y+2, 0, 1, 1)

    def OnButtonAddInput(self):
        print(len(self.master.device.inputs))
        self.AddInputRow()
        self.master.device.AddInputDefault()
        print(len(self.master.device.inputs))
        self.UpdateRowFromDeviceInput(len(self.master.device.inputs)-1)

    def AddInputRow(self):
        newRow = {}

        y = len(self.inputRows)+1
        new = CopyQComboBox(self.inputRow["comboPin"])
        newRow["comboPin"] = new
        self.container.addWidget(new, y, 0, 1, 1)
        new = CopyQComboBox(self.inputRow["comboPinMode"])
        newRow["comboPinMode"] = new
        self.container.addWidget(new, y, 1, 1, 1)
        new = CopyQComboBox(self.inputRow["comboAnalogDigital"])
        newRow["comboAnalogDigital"] = new
        self.container.addWidget(new, y, 2, 1, 1)
        new = CopyQCheckBox(self.inputRow["checkBoxInverted"])
        newRow["checkBoxInverted"] = new
        self.container.addWidget(new, y, 3, 1, 1)
        new = CopyQSpinBox(self.inputRow["spinMin"])
        newRow["spinMin"] = new
        self.container.addWidget(new, y, 4, 1, 1)
        new = CopyQSpinBox(self.inputRow["spinCenter"])
        newRow["spinCenter"] = new
        self.container.addWidget(new, y, 5, 1, 1)
        new = CopyQSpinBox(self.inputRow["spinMax"])
        newRow["spinMax"] = new
        self.container.addWidget(new, y, 6, 1, 1)
        new = CopyQSpinBox(self.inputRow["spinDeadZone"])
        newRow["spinDeadZone"] = new
        self.container.addWidget(new, y, 7, 1, 1)
        new = CopyQSpinBox(self.inputRow["spinFilterSize"])
        newRow["spinFilterSize"] = new
        self.container.addWidget(new, y, 8, 1, 1)
        new = CopyQComboBox(self.inputRow["comboBindingType"])
        newRow["comboBindingType"] = new
        self.container.addWidget(new, y, 9, 1, 1)
        new = CopyQComboBox(self.inputRow["comboBinding"])
        newRow["comboBinding"] = new
        self.container.addWidget(new, y, 10, 1, 1)
        new = CopyQLabel(self.inputRow["labelRawValue"])
        newRow["labelRawValue"] = new
        self.container.addWidget(new, y, 11, 1, 1)
        new = CopyQProgressBar(self.inputRow["progressBarValue"])
        newRow["progressBarValue"] = new
        self.container.addWidget(new, y, 12, 1, 1)
        new = CopyQButton(self.inputRow["buttonDelInputRow"])
        newRow["buttonDelInputRow"] = new
        self.container.addWidget(new, y, 13, 1, 1)

        newRow["comboPin"].currentTextChanged.connect(partial(self.OnPinChange, y-1))
        newRow["comboPinMode"].currentTextChanged.connect(partial(self.OnPinModeChange, y-1))
        newRow["comboAnalogDigital"].currentTextChanged.connect(partial(self.OnAnalogDigitalChange, y-1))
        newRow["checkBoxInverted"].stateChanged.connect(partial(self.OnInvertedChange, y-1))
        newRow["spinMin"].valueChanged.connect(partial(self.OnMinValChange, y-1))
        newRow["spinCenter"].valueChanged.connect(partial(self.OnMidValChange, y-1))
        newRow["spinMax"].valueChanged.connect(partial(self.OnMaxValChange, y-1))
        newRow["spinDeadZone"].valueChanged.connect(partial(self.OnDeadZoneChange, y-1))
        newRow["spinFilterSize"].valueChanged.connect(partial(self.OnFilterSizeChange, y-1))

        newRow["comboBindingType"].currentTextChanged.connect(partial(self.OnBindingTypeChanged, y-1))
        newRow["comboBinding"].currentTextChanged.connect(partial(self.OnBindingChanged, y-1))
        newRow["buttonDelInputRow"].clicked.connect(partial(self.RemoveInputRow, y-1))

        self.inputRows.append(newRow)

        self.buttonAddInput = self.master.buttonAddInput
        self.container.removeWidget(self.buttonAddInput)
        self.container.addWidget(self.buttonAddInput, y+1, 0, 1, 1)

        self.container.removeItem(self.master.verticalSpacer)
        self.container.addItem(self.master.verticalSpacer, y+2, 0, 1, 1)

        #self.OnBindingTypeChanged(y-1, sendUpdate=False)
        #self.master.device.AddInputDefault()

    def GetMasterInputRow(self):        
        row = {}
        row["comboPin"] = self.master.comboPin
        row["comboPinMode"] = self.master.comboPinMode
        row["comboAnalogDigital"] = self.master.comboAnalogDigital
        row["checkBoxInverted"] = self.master.checkBoxInverted
        row["spinMin"] = self.master.spinMin
        row["spinCenter"] = self.master.spinCenter
        row["spinMax"] = self.master.spinMax
        row["spinDeadZone"] = self.master.spinDeadZone
        row["spinFilterSize"] = self.master.spinFilterSize      
        row["comboBindingType"] = self.master.comboBindingType     
        row["comboBinding"] = self.master.comboBinding
        row["labelRawValue"] = self.master.labelRawValue
        row["progressBarValue"] = self.master.progressBarValue
        row["buttonDelInputRow"] = self.master.buttonDelInputRow

        return row


def CopyQComboBox(original):
    new = QComboBox()    
    new.addItems([original.itemText(i) for i in range(original.count())])
    return new


def CopyQCheckBox(original):
    new = QCheckBox()
    return new


def CopyQSpinBox(original):
    new = QSpinBox()
    new.setMinimum(original.minimum())
    new.setMaximum(original.maximum())
    new.setValue(original.value())
    return new


def CopyQLabel(original):
    new = QLabel()
    new.setText(original.text())
    return new


def CopyQButton(original):
    new = QPushButton()
    new.setText(original.text())
    new.setMaximumSize(original.maximumSize())
    new.setSizePolicy(original.sizePolicy())
    return new


def CopyQProgressBar(original):
    new = QProgressBar()
    new.setFont(original.font())
    new.setMaximum(original.maximum())  
    new.setMaximumSize(original.maximumSize())
    new.setFormat(original.format())
    new.setSizePolicy(original.sizePolicy())
    new.setAlignment(Qt.AlignCenter)
    new.setStyleSheet(" QProgressBar { border: 2px solid grey; border-radius: 3px; text-align: center; } QProgressBar::chunk {background-color: #3add36; width: 1px;}")
    return new