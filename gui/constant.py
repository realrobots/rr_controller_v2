from enum import Enum

# UI Pages of stackedWidget
PAGE_CONNECT = 0
PAGE_CONNECTED = 1

# HID Device Types
NONE = 0
GAMEPAD = 1
KEYBOARD = 2
MOUSE = 3
MACRO = 4  # refers to a combination of inputs

# HID Input Types
GAMEPAD_BUTTON = 1  # 1-128
GAMEPAD_AXIS = 2    # 0-7  x, y, z, rZ, rX, rY, slider1, slider2
GAMEPAD_HAT = 3     # 3 hats, 8 directions each, assignedInput = dir + hatIdx*9
GAMEPAD_SPECIAL = 4

inputTypes = ["NONE", "BUTTON", "AXIS", "HAT"]

inputsGamepadButtons = ["NONE"]
for i in range(1, 129):
    inputsGamepadButtons.append(str(i))
inputsGamepadAxes = ["X Axis", "Y Axis", "Z Axis", "rX Axis", "rY Axis", "rZ Axis", "Slider 1", "Slider 2"]
inputsGamepadHatDirections = ["NONE", "N", "NE", "E", "SE", "S", "SW", "W", "NW"]
inputsGamepadHats = []
for i in range(1, 5):
    for hatDir in inputsGamepadHatDirections:
        inputsGamepadHats.append("Hat " + str(i) + " " + hatDir)

def GetAssignmentList(inputType):
    indexOf = inputTypes.index(inputType)
    if indexOf == GAMEPAD_BUTTON:
        return inputsGamepadButtons
    elif indexOf == GAMEPAD_AXIS:
        return inputsGamepadAxes
    elif indexOf == GAMEPAD_HAT:
        return inputsGamepadHats

    else:
        return ["NONE"]

def GetAssignedInput(binding):
    inputType = binding.inputType
    assignedInput = binding.assignedInput
    if inputType == GAMEPAD_BUTTON:
        return inputsGamepadButtons[assignedInput]
    if inputType == GAMEPAD_AXIS:
        return inputsGamepadAxes[assignedInput]
    if inputType == GAMEPAD_HAT:
        return inputsGamepadHats[assignedInput]
    return "OOPS"


# Input States
BUTTON_UP = 0
BUTTON_DOWN = 1
BUTTON_ON_UP = 2
BUTTON_ON_DOWN = 3
CONSTANT = 4

# Device IDs
ESP32 = 0
ESP32S2 = 1
ESP32S3 = 2

microcontroller = ["ESP32", "ESP32S2", "ESP32S3"]

def GetMicrocontrollerType(device):
    return microcontroller[device.microcontroller]


DIGITAL = 0
ANALOG = 1
analogModes = ["DIGITAL", "ANALOG"]

# PinModes as defined by ESP32 

INPUT = 1
INPUT_PULLUP = 5
INPUT_PULLDOWN = 9
OUTPUT = 3

pinModes = ["INPUT", "INPUT_PULLUP", "INPUT_PULLDOWN", "OUTPUT"]


def IndexToPinMode(idx):
    if idx == 0:
        return INPUT
    if idx == 1:
        return INPUT_PULLUP
    if idx == 2:
        return INPUT_PULLDOWN
    if idx == 3:
        return OUTPUT


def PinModeToIndex(pinMode):
    if pinMode == INPUT:
        return 0
    if pinMode == INPUT_PULLUP:
        return 1
    if pinMode == INPUT_PULLDOWN:
        return 2
    if pinMode == OUTPUT:
        return 3




def GetDeviceBlueprint(deviceID):
    if deviceID == ESP32:
        import deviceTargets.target_esp32
        return deviceTargets.target_esp32
    elif deviceID == ESP32S2:
        import deviceTargets.target_esp32
        return deviceTargets.target_esp32
    elif deviceID == ESP32S3:
        import deviceTargets.target_esp32
        return deviceTargets.target_esp32
    else:
        return None

def SplitBytes(bigVal):
    val0 = (bigVal >> 8) & 0xff
    val1 = bigVal & 0xff
    return [val0, val1]