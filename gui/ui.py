# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'test1BCsHPW.ui'
##
## Created by: Qt User Interface Compiler version 5.15.3
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *

#import test_rc

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(1030, 646)
        sizePolicy = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(MainWindow.sizePolicy().hasHeightForWidth())
        MainWindow.setSizePolicy(sizePolicy)
        self.actionOne = QAction(MainWindow)
        self.actionOne.setObjectName(u"actionOne")
        self.actionTwo = QAction(MainWindow)
        self.actionTwo.setObjectName(u"actionTwo")
        self.actionThree = QAction(MainWindow)
        self.actionThree.setObjectName(u"actionThree")
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        sizePolicy.setHeightForWidth(self.centralwidget.sizePolicy().hasHeightForWidth())
        self.centralwidget.setSizePolicy(sizePolicy)
        self.textOutput = QTextBrowser(self.centralwidget)
        self.textOutput.setObjectName(u"textOutput")
        self.textOutput.setGeometry(QRect(335, 441, 681, 171))
        sizePolicy1 = QSizePolicy(QSizePolicy.Maximum, QSizePolicy.Expanding)
        sizePolicy1.setHorizontalStretch(99)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.textOutput.sizePolicy().hasHeightForWidth())
        self.textOutput.setSizePolicy(sizePolicy1)
        self.stackedWidget = QStackedWidget(self.centralwidget)
        self.stackedWidget.setObjectName(u"stackedWidget")
        self.stackedWidget.setGeometry(QRect(0, -1, 1021, 441))
        sizePolicy.setHeightForWidth(self.stackedWidget.sizePolicy().hasHeightForWidth())
        self.stackedWidget.setSizePolicy(sizePolicy)
        self.page = QWidget()
        self.page.setObjectName(u"page")
        self.frameConnect = QFrame(self.page)
        self.frameConnect.setObjectName(u"frameConnect")
        self.frameConnect.setEnabled(True)
        self.frameConnect.setGeometry(QRect(60, 50, 891, 131))
        self.frameConnect.setFrameShape(QFrame.StyledPanel)
        self.frameConnect.setFrameShadow(QFrame.Raised)
        self.gridLayoutWidget = QWidget(self.frameConnect)
        self.gridLayoutWidget.setObjectName(u"gridLayoutWidget")
        self.gridLayoutWidget.setGeometry(QRect(0, 0, 891, 131))
        self.gridLayoutConnect = QGridLayout(self.gridLayoutWidget)
        self.gridLayoutConnect.setObjectName(u"gridLayoutConnect")
        self.gridLayoutConnect.setContentsMargins(0, 0, 0, 0)
        self.comboDeviceType = QComboBox(self.gridLayoutWidget)
        self.comboDeviceType.addItem("")
        self.comboDeviceType.setObjectName(u"comboDeviceType")

        self.gridLayoutConnect.addWidget(self.comboDeviceType, 3, 1, 1, 1)

        self.buttonConnect = QPushButton(self.gridLayoutWidget)
        self.buttonConnect.setObjectName(u"buttonConnect")

        self.gridLayoutConnect.addWidget(self.buttonConnect, 2, 2, 1, 1)

        self.buttonUpload = QPushButton(self.gridLayoutWidget)
        self.buttonUpload.setObjectName(u"buttonUpload")

        self.gridLayoutConnect.addWidget(self.buttonUpload, 3, 2, 1, 1)

        self.comboPort = QComboBox(self.gridLayoutWidget)
        self.comboPort.addItem("")
        self.comboPort.addItem("")
        self.comboPort.setObjectName(u"comboPort")
        sizePolicy2 = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Fixed)
        sizePolicy2.setHorizontalStretch(255)
        sizePolicy2.setVerticalStretch(0)
        sizePolicy2.setHeightForWidth(self.comboPort.sizePolicy().hasHeightForWidth())
        self.comboPort.setSizePolicy(sizePolicy2)

        self.gridLayoutConnect.addWidget(self.comboPort, 1, 1, 1, 1)

        self.buttonRefresh = QPushButton(self.gridLayoutWidget)
        self.buttonRefresh.setObjectName(u"buttonRefresh")

        self.gridLayoutConnect.addWidget(self.buttonRefresh, 1, 2, 1, 1)

        self.label = QLabel(self.gridLayoutWidget)
        self.label.setObjectName(u"label")
        self.label.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayoutConnect.addWidget(self.label, 1, 0, 1, 1)

        self.label_2 = QLabel(self.gridLayoutWidget)
        self.label_2.setObjectName(u"label_2")
        self.label_2.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayoutConnect.addWidget(self.label_2, 3, 0, 1, 1)

        self.stackedWidget.addWidget(self.page)
        self.page_2 = QWidget()
        self.page_2.setObjectName(u"page_2")
        sizePolicy.setHeightForWidth(self.page_2.sizePolicy().hasHeightForWidth())
        self.page_2.setSizePolicy(sizePolicy)
        self.gridLayoutWidget_2 = QWidget(self.page_2)
        self.gridLayoutWidget_2.setObjectName(u"gridLayoutWidget_2")
        self.gridLayoutWidget_2.setGeometry(QRect(9, 10, 1011, 421))
        self.gridInputList = QGridLayout(self.gridLayoutWidget_2)
        self.gridInputList.setObjectName(u"gridInputList")
        self.gridInputList.setHorizontalSpacing(11)
        self.gridInputList.setContentsMargins(0, 0, 0, 0)
        self.label_9 = QLabel(self.gridLayoutWidget_2)
        self.label_9.setObjectName(u"label_9")
        self.label_9.setAlignment(Qt.AlignCenter)

        self.gridInputList.addWidget(self.label_9, 0, 5, 1, 1)

        self.label_13 = QLabel(self.gridLayoutWidget_2)
        self.label_13.setObjectName(u"label_13")
        self.label_13.setAlignment(Qt.AlignCenter)

        self.gridInputList.addWidget(self.label_13, 0, 11, 1, 1)

        self.label_16 = QLabel(self.gridLayoutWidget_2)
        self.label_16.setObjectName(u"label_16")
        self.label_16.setAlignment(Qt.AlignCenter)

        self.gridInputList.addWidget(self.label_16, 0, 10, 1, 1)

        self.label_14 = QLabel(self.gridLayoutWidget_2)
        self.label_14.setObjectName(u"label_14")
        self.label_14.setAlignment(Qt.AlignCenter)

        self.gridInputList.addWidget(self.label_14, 0, 12, 1, 1)

        self.label_12 = QLabel(self.gridLayoutWidget_2)
        self.label_12.setObjectName(u"label_12")
        self.label_12.setAlignment(Qt.AlignCenter)

        self.gridInputList.addWidget(self.label_12, 0, 8, 1, 1)

        self.checkBoxInverted = QCheckBox(self.gridLayoutWidget_2)
        self.checkBoxInverted.setObjectName(u"checkBoxInverted")
        self.checkBoxInverted.setLayoutDirection(Qt.LeftToRight)
        self.checkBoxInverted.setTristate(False)

        self.gridInputList.addWidget(self.checkBoxInverted, 1, 3, 1, 1)

        self.label_4 = QLabel(self.gridLayoutWidget_2)
        self.label_4.setObjectName(u"label_4")
        self.label_4.setAlignment(Qt.AlignCenter)

        self.gridInputList.addWidget(self.label_4, 0, 0, 1, 1)

        self.spinFilterSize = QSpinBox(self.gridLayoutWidget_2)
        self.spinFilterSize.setObjectName(u"spinFilterSize")
        self.spinFilterSize.setMaximum(16)

        self.gridInputList.addWidget(self.spinFilterSize, 1, 8, 1, 1)

        self.label_7 = QLabel(self.gridLayoutWidget_2)
        self.label_7.setObjectName(u"label_7")
        self.label_7.setAlignment(Qt.AlignCenter)

        self.gridInputList.addWidget(self.label_7, 0, 3, 1, 1)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.gridInputList.addItem(self.verticalSpacer, 3, 0, 1, 1)

        self.comboAnalogDigital = QComboBox(self.gridLayoutWidget_2)
        self.comboAnalogDigital.addItem("")
        self.comboAnalogDigital.addItem("")
        self.comboAnalogDigital.setObjectName(u"comboAnalogDigital")
        font = QFont()
        font.setPointSize(8)
        self.comboAnalogDigital.setFont(font)

        self.gridInputList.addWidget(self.comboAnalogDigital, 1, 2, 1, 1)

        self.spinMin = QSpinBox(self.gridLayoutWidget_2)
        self.spinMin.setObjectName(u"spinMin")
        self.spinMin.setFont(font)
        self.spinMin.setMaximum(4096)

        self.gridInputList.addWidget(self.spinMin, 1, 4, 1, 1)

        self.buttonDelInputRow = QPushButton(self.gridLayoutWidget_2)
        self.buttonDelInputRow.setObjectName(u"buttonDelInputRow")
        sizePolicy3 = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Fixed)
        sizePolicy3.setHorizontalStretch(0)
        sizePolicy3.setVerticalStretch(0)
        sizePolicy3.setHeightForWidth(self.buttonDelInputRow.sizePolicy().hasHeightForWidth())
        self.buttonDelInputRow.setSizePolicy(sizePolicy3)
        self.buttonDelInputRow.setMaximumSize(QSize(32, 16777215))

        self.gridInputList.addWidget(self.buttonDelInputRow, 1, 13, 1, 1)

        self.label_15 = QLabel(self.gridLayoutWidget_2)
        self.label_15.setObjectName(u"label_15")
        self.label_15.setAlignment(Qt.AlignCenter)

        self.gridInputList.addWidget(self.label_15, 0, 9, 1, 1)

        self.labelRawValue = QLabel(self.gridLayoutWidget_2)
        self.labelRawValue.setObjectName(u"labelRawValue")
        self.labelRawValue.setFont(font)

        self.gridInputList.addWidget(self.labelRawValue, 1, 11, 1, 1)

        self.spinDeadZone = QSpinBox(self.gridLayoutWidget_2)
        self.spinDeadZone.setObjectName(u"spinDeadZone")
        self.spinDeadZone.setFont(font)
        self.spinDeadZone.setMaximum(4096)
        self.spinDeadZone.setValue(512)

        self.gridInputList.addWidget(self.spinDeadZone, 1, 7, 1, 1)

        self.spinCenter = QSpinBox(self.gridLayoutWidget_2)
        self.spinCenter.setObjectName(u"spinCenter")
        self.spinCenter.setFont(font)
        self.spinCenter.setMaximum(2047)
        self.spinCenter.setValue(2047)

        self.gridInputList.addWidget(self.spinCenter, 1, 5, 1, 1)

        self.label_11 = QLabel(self.gridLayoutWidget_2)
        self.label_11.setObjectName(u"label_11")
        self.label_11.setAlignment(Qt.AlignCenter)

        self.gridInputList.addWidget(self.label_11, 0, 7, 1, 1)

        self.label_8 = QLabel(self.gridLayoutWidget_2)
        self.label_8.setObjectName(u"label_8")
        self.label_8.setAlignment(Qt.AlignCenter)

        self.gridInputList.addWidget(self.label_8, 0, 4, 1, 1)

        self.label_10 = QLabel(self.gridLayoutWidget_2)
        self.label_10.setObjectName(u"label_10")
        self.label_10.setAlignment(Qt.AlignCenter)

        self.gridInputList.addWidget(self.label_10, 0, 6, 1, 1)

        self.comboPinMode = QComboBox(self.gridLayoutWidget_2)
        self.comboPinMode.addItem("")
        self.comboPinMode.addItem("")
        self.comboPinMode.addItem("")
        self.comboPinMode.addItem("")
        self.comboPinMode.setObjectName(u"comboPinMode")
        self.comboPinMode.setFont(font)

        self.gridInputList.addWidget(self.comboPinMode, 1, 1, 1, 1)

        self.label_5 = QLabel(self.gridLayoutWidget_2)
        self.label_5.setObjectName(u"label_5")
        self.label_5.setAlignment(Qt.AlignCenter)

        self.gridInputList.addWidget(self.label_5, 0, 1, 1, 1)

        self.comboBinding = QComboBox(self.gridLayoutWidget_2)
        self.comboBinding.addItem("")
        self.comboBinding.addItem("")
        self.comboBinding.setObjectName(u"comboBinding")
        self.comboBinding.setFont(font)

        self.gridInputList.addWidget(self.comboBinding, 1, 10, 1, 1)

        self.label_6 = QLabel(self.gridLayoutWidget_2)
        self.label_6.setObjectName(u"label_6")
        self.label_6.setAlignment(Qt.AlignCenter)

        self.gridInputList.addWidget(self.label_6, 0, 2, 1, 1)

        self.spinMax = QSpinBox(self.gridLayoutWidget_2)
        self.spinMax.setObjectName(u"spinMax")
        self.spinMax.setFont(font)
        self.spinMax.setMaximum(4095)
        self.spinMax.setValue(4095)

        self.gridInputList.addWidget(self.spinMax, 1, 6, 1, 1)

        self.buttonAddInput = QPushButton(self.gridLayoutWidget_2)
        self.buttonAddInput.setObjectName(u"buttonAddInput")
        self.buttonAddInput.setFont(font)

        self.gridInputList.addWidget(self.buttonAddInput, 2, 0, 1, 1)

        self.comboPin = QComboBox(self.gridLayoutWidget_2)
        self.comboPin.addItem("")
        self.comboPin.addItem("")
        self.comboPin.addItem("")
        self.comboPin.setObjectName(u"comboPin")

        self.gridInputList.addWidget(self.comboPin, 1, 0, 1, 1)

        self.comboBindingType = QComboBox(self.gridLayoutWidget_2)
        self.comboBindingType.addItem("")
        self.comboBindingType.addItem("")
        self.comboBindingType.setObjectName(u"comboBindingType")
        self.comboBindingType.setFont(font)

        self.gridInputList.addWidget(self.comboBindingType, 1, 9, 1, 1)

        self.progressBarValue = QProgressBar(self.gridLayoutWidget_2)
        self.progressBarValue.setObjectName(u"progressBarValue")
        self.progressBarValue.setMaximumSize(QSize(62, 16777215))
        self.progressBarValue.setFont(font)
        self.progressBarValue.setMaximum(32767)
        self.progressBarValue.setValue(16000)

        self.gridInputList.addWidget(self.progressBarValue, 1, 12, 1, 1)

        self.stackedWidget.addWidget(self.page_2)
        self.page_3 = QWidget()
        self.page_3.setObjectName(u"page_3")
        self.stackedWidget.addWidget(self.page_3)
        self.gridLayoutWidget_3 = QWidget(self.centralwidget)
        self.gridLayoutWidget_3.setObjectName(u"gridLayoutWidget_3")
        self.gridLayoutWidget_3.setGeometry(QRect(50, 449, 281, 152))
        self.gridLayout = QGridLayout(self.gridLayoutWidget_3)
        self.gridLayout.setObjectName(u"gridLayout")
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.labelDeviceName = QLabel(self.gridLayoutWidget_3)
        self.labelDeviceName.setObjectName(u"labelDeviceName")

        self.gridLayout.addWidget(self.labelDeviceName, 1, 1, 1, 1)

        self.labelMicrocontroller = QLabel(self.gridLayoutWidget_3)
        self.labelMicrocontroller.setObjectName(u"labelMicrocontroller")

        self.gridLayout.addWidget(self.labelMicrocontroller, 2, 1, 1, 1)

        self.label_20 = QLabel(self.gridLayoutWidget_3)
        self.label_20.setObjectName(u"label_20")

        self.gridLayout.addWidget(self.label_20, 3, 0, 1, 1)

        self.labelFirmwareVersion = QLabel(self.gridLayoutWidget_3)
        self.labelFirmwareVersion.setObjectName(u"labelFirmwareVersion")

        self.gridLayout.addWidget(self.labelFirmwareVersion, 3, 1, 1, 1)

        self.buttonSendUpdate = QPushButton(self.gridLayoutWidget_3)
        self.buttonSendUpdate.setObjectName(u"buttonSendUpdate")

        self.gridLayout.addWidget(self.buttonSendUpdate, 4, 1, 1, 1)

        self.buttonDisconnect = QPushButton(self.gridLayoutWidget_3)
        self.buttonDisconnect.setObjectName(u"buttonDisconnect")

        self.gridLayout.addWidget(self.buttonDisconnect, 4, 0, 1, 1)

        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.gridLayout.addItem(self.horizontalSpacer, 4, 2, 1, 1)

        self.label_3 = QLabel(self.gridLayoutWidget_3)
        self.label_3.setObjectName(u"label_3")

        self.gridLayout.addWidget(self.label_3, 0, 0, 1, 1)

        self.label_18 = QLabel(self.gridLayoutWidget_3)
        self.label_18.setObjectName(u"label_18")

        self.gridLayout.addWidget(self.label_18, 1, 0, 1, 1)

        self.labelIsConnected = QLabel(self.gridLayoutWidget_3)
        self.labelIsConnected.setObjectName(u"labelIsConnected")

        self.gridLayout.addWidget(self.labelIsConnected, 0, 1, 1, 1)

        self.label_19 = QLabel(self.gridLayoutWidget_3)
        self.label_19.setObjectName(u"label_19")

        self.gridLayout.addWidget(self.label_19, 2, 0, 1, 1)

        self.checkBoxValUpdates = QCheckBox(self.gridLayoutWidget_3)
        self.checkBoxValUpdates.setObjectName(u"checkBoxValUpdates")
        self.checkBoxValUpdates.setChecked(True)

        self.gridLayout.addWidget(self.checkBoxValUpdates, 5, 0, 1, 1)

        self.buttonSaveToFlash = QPushButton(self.gridLayoutWidget_3)
        self.buttonSaveToFlash.setObjectName(u"buttonSaveToFlash")

        self.gridLayout.addWidget(self.buttonSaveToFlash, 5, 1, 1, 1)

        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QMenuBar(MainWindow)
        self.menubar.setObjectName(u"menubar")
        self.menubar.setGeometry(QRect(0, 0, 1030, 22))
        self.menuFile = QMenu(self.menubar)
        self.menuFile.setObjectName(u"menuFile")
        self.menuEdit = QMenu(self.menubar)
        self.menuEdit.setObjectName(u"menuEdit")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QStatusBar(MainWindow)
        self.statusbar.setObjectName(u"statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.menubar.addAction(self.menuFile.menuAction())
        self.menubar.addAction(self.menuEdit.menuAction())
        self.menuFile.addAction(self.actionOne)
        self.menuFile.addAction(self.actionTwo)
        self.menuFile.addAction(self.actionThree)

        self.retranslateUi(MainWindow)

        self.stackedWidget.setCurrentIndex(0)


        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"RealRobots HID Configurator", None))
        self.actionOne.setText(QCoreApplication.translate("MainWindow", u"One", None))
        self.actionTwo.setText(QCoreApplication.translate("MainWindow", u"Two", None))
        self.actionThree.setText(QCoreApplication.translate("MainWindow", u"Three", None))
        self.comboDeviceType.setItemText(0, QCoreApplication.translate("MainWindow", u"ESP32 - Bluetooth BLE HID", None))

        self.buttonConnect.setText(QCoreApplication.translate("MainWindow", u"Connect", None))
        self.buttonUpload.setText(QCoreApplication.translate("MainWindow", u"Upload Firmware", None))
        self.comboPort.setItemText(0, QCoreApplication.translate("MainWindow", u"COM3", None))
        self.comboPort.setItemText(1, QCoreApplication.translate("MainWindow", u"COM4", None))

        self.buttonRefresh.setText(QCoreApplication.translate("MainWindow", u"Refresh", None))
        self.label.setText(QCoreApplication.translate("MainWindow", u"Select Device:", None))
        self.label_2.setText(QCoreApplication.translate("MainWindow", u"Device Type:", None))
        self.label_9.setText(QCoreApplication.translate("MainWindow", u"Center", None))
        self.label_13.setText(QCoreApplication.translate("MainWindow", u"Raw", None))
        self.label_16.setText(QCoreApplication.translate("MainWindow", u"Binding", None))
        self.label_14.setText(QCoreApplication.translate("MainWindow", u"Calibrated", None))
        self.label_12.setText(QCoreApplication.translate("MainWindow", u"FilterSize", None))
        self.checkBoxInverted.setText("")
        self.label_4.setText(QCoreApplication.translate("MainWindow", u"Pin", None))
        self.label_7.setText(QCoreApplication.translate("MainWindow", u"Inverted", None))
        self.comboAnalogDigital.setItemText(0, QCoreApplication.translate("MainWindow", u"DIGITAL", None))
        self.comboAnalogDigital.setItemText(1, QCoreApplication.translate("MainWindow", u"ANALOG", None))

        self.buttonDelInputRow.setText(QCoreApplication.translate("MainWindow", u"X", None))
        self.label_15.setText(QCoreApplication.translate("MainWindow", u"Type", None))
        self.labelRawValue.setText(QCoreApplication.translate("MainWindow", u"-1", None))
        self.label_11.setText(QCoreApplication.translate("MainWindow", u"DeadZone", None))
        self.label_8.setText(QCoreApplication.translate("MainWindow", u"Min", None))
        self.label_10.setText(QCoreApplication.translate("MainWindow", u"Max", None))
        self.comboPinMode.setItemText(0, QCoreApplication.translate("MainWindow", u"INPUT", None))
        self.comboPinMode.setItemText(1, QCoreApplication.translate("MainWindow", u"PULLUP", None))
        self.comboPinMode.setItemText(2, QCoreApplication.translate("MainWindow", u"PULLDOWN", None))
        self.comboPinMode.setItemText(3, QCoreApplication.translate("MainWindow", u"OUTPUT", None))

        self.label_5.setText(QCoreApplication.translate("MainWindow", u"PinMode", None))
        self.comboBinding.setItemText(0, QCoreApplication.translate("MainWindow", u"X Axis", None))
        self.comboBinding.setItemText(1, QCoreApplication.translate("MainWindow", u"Button 1", None))

        self.label_6.setText(QCoreApplication.translate("MainWindow", u"Analog/Digital", None))
        self.buttonAddInput.setText(QCoreApplication.translate("MainWindow", u"Add Input", None))
        self.comboPin.setItemText(0, QCoreApplication.translate("MainWindow", u"0", None))
        self.comboPin.setItemText(1, QCoreApplication.translate("MainWindow", u"1", None))
        self.comboPin.setItemText(2, QCoreApplication.translate("MainWindow", u"2", None))

        self.comboBindingType.setItemText(0, QCoreApplication.translate("MainWindow", u"X Axis", None))
        self.comboBindingType.setItemText(1, QCoreApplication.translate("MainWindow", u"Button 1", None))

        self.progressBarValue.setFormat(QCoreApplication.translate("MainWindow", u"%v", None))
        self.labelDeviceName.setText("")
        self.labelMicrocontroller.setText("")
        self.label_20.setText(QCoreApplication.translate("MainWindow", u"Firmware Version:", None))
        self.labelFirmwareVersion.setText("")
        self.buttonSendUpdate.setText(QCoreApplication.translate("MainWindow", u"Update Configuration", None))
        self.buttonDisconnect.setText(QCoreApplication.translate("MainWindow", u"Disconnect", None))
        self.label_3.setText(QCoreApplication.translate("MainWindow", u"Connected:", None))
        self.label_18.setText(QCoreApplication.translate("MainWindow", u"Device Name:", None))
        self.labelIsConnected.setText(QCoreApplication.translate("MainWindow", u"False", None))
        self.label_19.setText(QCoreApplication.translate("MainWindow", u"Microcontroller:", None))
        self.checkBoxValUpdates.setText(QCoreApplication.translate("MainWindow", u"Update Values", None))
#if QT_CONFIG(tooltip)
        self.buttonSaveToFlash.setToolTip(QCoreApplication.translate("MainWindow", u"Store changes permanently\n"
"Otherwise changes will be lost when your device loses power", None))
#endif // QT_CONFIG(tooltip)
        self.buttonSaveToFlash.setText(QCoreApplication.translate("MainWindow", u"Save to FLASH", None))
        self.menuFile.setTitle(QCoreApplication.translate("MainWindow", u"File", None))
        self.menuEdit.setTitle(QCoreApplication.translate("MainWindow", u"Edit", None))
    # retranslateUi

