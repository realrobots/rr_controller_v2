from PySide2 import QtWidgets, QtCore, QtGui
from PySide2.QtCore import QRunnable, Slot, QThreadPool, QObject, QThread,QByteArray
from PySide2.QtCore import Signal, Slot
import ser
import ui
import upload
import constant
import sys
import time
import page_connect
import page_input_list
import threading
from functools import partial

import device

HIJACK_STDOUT = True


class WorkerConsole(QRunnable):
    def __init__(self):
        super().__init__()
        self.is_running = False

    def Go(self, textBox):
        self.textBox = textBox
        sys.stdout = self
        sys.stderr = self

    @Slot()
    def run(self):
        print("Thread start")

        print("Thread complete")
        self.is_running = False

    def write(self, text):
        self.textBox.insertPlainText(text)
        self.textBox.moveCursor(QtGui.QTextCursor.End)

    def flush(self):
        pass

    def isatty(self):
        pass

    def GetIsRunning(self):
        return self.is_running

class SerialWorker(QRunnable):
    def __init__(self):
        super().__init__()
        self.is_running = False

    def assignConnection(self, conn):
        self.conn = conn

    @Slot()
    def run(self):
        print("Thread start")

        self.is_running = True
        while True:
            self.conn.update()
        print("Thread complete")
        self.is_running = False

    def GetIsRunning(self):
        return self.is_running


# Handles calls to esptool and passes stdout/stderr to textbox
class UploadWorker(QRunnable):   

    def __init__(self, master):
        super().__init__()
        self.is_running = False
        self.master = master

    def set_port(self, port):
        self.port = port

    @Slot()
    def run(self):
        print("Thread start")

        self.is_running = True
        try:
            upload.erase_flash(self.port)
            upload.flash_firmware(self.port)
        except Exception as ex:
            template = "An exception of type {0} occurred.\n{1!r}"
            message = template.format(type(ex).__name__, ex.args)
            print(message)
        print("Thread complete")
        self.is_running = False
        self.master.upload_finished()

    def GetIsRunning(self):
        return self.is_running

class Worker(QObject):
    finished = Signal()
    update = Signal(int)


    @Slot()
    def procCounter(self):
        count = 0
        while True:
            time.sleep(0.1)
            self.update.emit(count)
            count += 1

        self.finished.emit()



class MyQtApp(ui.Ui_MainWindow, QtWidgets.QMainWindow):
    def __init__(self):
        super(MyQtApp, self).__init__()
        self.setupUi(self)
        self.setFixedSize(1028, 655)
        self.conn = ser.SerialConnection(self)
        self.device = None
        
        self.connectPage = page_connect.ConnectPage(self)
        self.inputListPage = page_input_list.InputListPage(self)
        

        self.threadpool = QThreadPool()

        if HIJACK_STDOUT:
            self.workerConsole = WorkerConsole()
            self.workerConsole.setAutoDelete(False)
            self.workerConsole.Go(self.textOutput)

        self.worker = UploadWorker(self)
        #self.worker.set_target(self.textOutput)
        #self.worker.setAutoDelete(False)

        self.obj = Worker()
        self.thread = QThread()
        self.obj.update.connect(self.update)
        self.obj.moveToThread(self.thread)
        self.obj.finished.connect(self.thread.quit)
        self.thread.started.connect(self.obj.procCounter)
        self.thread.start()
        
        self.buttonDisconnect.clicked.connect(self.conn.disconnect)
        self.buttonSendUpdate.clicked.connect(partial(self.conn.sendDeviceUpdate, 0))
        self.checkBoxValUpdates.stateChanged.connect(self.ToggleSendValues)
        self.buttonSaveToFlash.clicked.connect(self.conn.sendSaveToFlash)

        self.buttonDisconnect.hide()
        self.buttonSendUpdate.hide()
        self.checkBoxValUpdates.hide()
        self.buttonSaveToFlash.hide()

        print("Choose your ESP32 from the port list")
        print("First time you'll have to upload the firmware")
        print("After firmware is flashed, pair with your computer via bluetooth")
        print("If you were already paired you'll have to forget and pair again")
        print('\n')
        print("Press CONNECT to enter configuration interface")


    def main_loop(self):
        print(self.current_device)

        threading.Timer(.1, self.main_loop).start()

    def upload_finished(self):
        self.connectPage.disable_inputs(False)

    def update(self):
        self.conn.update()
            
        pass


    def SetUIPage(self, pageIndex):
        self.stackedWidget.setCurrentIndex(pageIndex)

    def OnDeviceConnected(self):
        self.inputListPage.UpdateInputsFromDevice()
        self.labelIsConnected.setText("True")
        self.labelDeviceName.setText(self.device.deviceName)
        self.labelMicrocontroller.setText(constant.GetMicrocontrollerType(self.device))
        self.labelFirmwareVersion.setText(str(self.device.firmwareVersion))
        self.buttonDisconnect.show()
        #self.buttonSendUpdate.show()
        self.checkBoxValUpdates.show()
        self.buttonSaveToFlash.show()
        print("connected")

    def ToggleSendValues(self, val):
        print(val)
        if val == 2:
            self.conn.enableInputValueRequests()
        else:
            self.conn.disableInputValueRequests()

    # def keyPressEvent(self, event):
    #     print(event.key())
    #     if event.key() == 71: # g
    #         self.conn.enableInputValueRequests()
        
    #     if event.key() == 72: # h
    #         self.conn.disableInputValueRequests()
        


if __name__ == '__main__':  
    app = QtWidgets.QApplication()
    qt_app = MyQtApp()
    qt_app.show()
    app.exec_()