import ser
import constant
import upload

class ConnectPage():
    def __init__(self, master):
        self.master = master
        self.container = master.frameConnect
        self.master.buttonRefresh.clicked.connect(self.refresh_com_ports)
        self.master.buttonConnect.clicked.connect(self.try_connect)
        self.master.buttonUpload.clicked.connect(self.try_upload)
        self.refresh_com_ports()
        self.master.textOutput.clear()
        self.master.textOutput.insertPlainText("Console Output\n")

    def disable_inputs(self, state=True):
        self.master.buttonRefresh.setDisabled(state)
        self.master.buttonConnect.setDisabled(state)
        self.master.buttonUpload.setDisabled(state)
        self.master.comboPort.setDisabled(state)
        self.master.comboDeviceType.setDisabled(state)

    def try_connect(self):
        selectedPort = self.master.comboPort.currentText().split()[0]
        print("Attempting to connect to HID Device on " + str(selectedPort) + "...")
        #self.master.SetUIPage(constant.PAGE_CONNECTED)    
        self.master.conn.try_connect("COM4")

    def refresh_com_ports(self):
        print("refresh com ports")
        ports = ser.get_serial_ports()
        self.master.comboPort.clear()
        print(str(len(ports)) + " ports found")
        for i in range(0, len(ports)):
            print(ports[i])
            self.master.comboPort.addItem(ports[i])

    
    def try_upload(self):
        # upload.erase_flash(self.comboPort.currentText().split()[0])
        # upload.flash_firmware(self.comboPort.currentText().split()[0])
        # print(self.comboBox.currentText().split()[0])
        if not self.master.worker.GetIsRunning():
            print("Flashing firmware")
            #self.disable_inputs()
            selectedPort = self.master.comboPort.currentText().split()[0]
            print("Attempting to upload firmware to " + str(selectedPort) + "...")
            self.disable_inputs(True)
            self.master.worker.set_port(selectedPort)
            self.master.threadpool.start(self.master.worker)
            #upload.flash_firmware("COM4")
        else:
            print("ALREADY RUNNING")
        #upload.flash_firmware("COM4")