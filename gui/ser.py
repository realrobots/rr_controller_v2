
from PySide2 import QtWidgets, QtSerialPort
from PySide2.QtCore import QObject, QThread, QIODevice
from enum import Enum
import sys
import glob
import serial
import time
import constant
import device

from serial.tools.list_ports import comports

INPUT_VALUE_UPDATE_DELAY = 0.1

OUTGOING_REQUEST = 36  # '$'
OUTGOING_REQUEST_DEVICE_CONFIG = 100
INCOMING_RESPONSE_DEVICE_CONFIG = 101
OUTGOING_REQUEST_INPUT_CONFIG = 102
INCOMING_RESPONSE_INPUT_CONFIG = 103
OUTGOING_REQUEST_INPUT_ALL_CONFIG = 104
INCOMING_RESPONSE_INPUT_ALL_CONFIG = 105

OUTGOING_SEND_INPUT_CONFIG_UPDATE = 106
INCOMING_RESPONSE_RECEIVED_INPUT_CONFIG_UPDATE = 107

OUTGOING_DELETE_INPUT_UPDATE = 108
INCOMING_RESPONSE_DELETE_INPUT_UPDATE = 109

OUTGOING_REQUEST_INPUT_VALUES = 110
INCOMING_RESPONSE_INPUT_VALUES = 111

OUTGOING_REQUEST_SAVE_TO_FLASH = 112
INCOMING_RESPONSE_SAVE_TO_FLASH = 113


class SerAction(Enum):
    DISCONNECTED = 0
    WAITING_ON_HANDSHAKE = 1
    CONNECTED = 2
    WAITING_ON_DEVICE_CONFIG = 3
    WAITING_ON_INPUT_CONFIG = 4
    WAITING_ON_INPUT_ALL_CONFIG = 5
    WAITING_ON_CONFIG_SEND_UPDATE_CONFIRMATION = 6
    WAITING_ON_CONFIG_SEND_UPDATE_CONFIRMATION_ALL = 7
    WAITING_ON_DELETE_INPUT_CONFIRMATION = 8
    WAITING_ON_INPUT_VALUES = 9
    WAITING_ON_SAVE_TO_FLASH = 10


HANDSHAKE_TIMEOUT = 2


class SerialConnection:
    def __init__(self, master):
        self.action = SerAction.DISCONNECTED
        self.actionStartTime = 0
        self.master = master

        self.updateInputValues = True

        self.char_buffer = bytearray()
        self.eol = bytearray()
        self.eol.append(ord('\r'))
        self.eol.append(ord('\n'))

        self.lastSent = bytearray()

        hsr = "$HELLO THERE\r\n"
        self.handshakeRequest = to_byte_array(hsr)
        hsr = "Hello yourself!"
        self.handshakeResponse = to_byte_array(hsr)

        self.queue = []

        self.serial = QtSerialPort.QSerialPort(
            '??',
            baudRate=115200
            
        )

        # print(self.serial)


    def disconnect(self):
        print("disconnect not implemented yet")

    def enableInputValueRequests(self):
        self.updateInputValues = True
        print("enabling input value updates")

    def disableInputValueRequests(self):
        self.updateInputValues = False
        print("disabling input value updates")

    def sendDeviceUpdate(self, inputIdx, sendAll=False):        
        print("sending device update")
        bytes = bytearray()
        bytes.append(OUTGOING_REQUEST)
        bytes.append(OUTGOING_SEND_INPUT_CONFIG_UPDATE)
        deviceBytes = self.master.device.inputs[inputIdx].ToBytes()
        
        bytes.append(inputIdx)

        for b in deviceBytes:
            bytes.append(b)
        for b in self.eol:
            bytes.append(b)

        #self.write(bytes)
        if sendAll:
            self.start_action(SerAction.WAITING_ON_CONFIG_SEND_UPDATE_CONFIRMATION_ALL, bytes)
        else:
            self.start_action(SerAction.WAITING_ON_CONFIG_SEND_UPDATE_CONFIRMATION, bytes)

    def sendDeleteInput(self, inputIdx):
        if self.action != SerAction.CONNECTED:
            return
        print("sending delte input update")
        bytes = bytearray()
        bytes.append(OUTGOING_REQUEST)
        bytes.append(OUTGOING_DELETE_INPUT_UPDATE)
        bytes.append(inputIdx)
        for b in self.eol:
            bytes.append(b)

        #self.write(bytes)
        self.start_action(SerAction.WAITING_ON_DELETE_INPUT_CONFIRMATION, bytes)

    def sendSaveToFlash(self):
        print("sending request: save to flash")
        bytes = bytearray()
        bytes.append(OUTGOING_REQUEST)
        bytes.append(OUTGOING_REQUEST_SAVE_TO_FLASH)
        for b in self.eol:
            bytes.append(b)

        #self.write(bytes)
        self.start_action(SerAction.WAITING_ON_SAVE_TO_FLASH, bytes)

    def requestInputValues(self):
        bytes = bytearray()
        bytes.append(OUTGOING_REQUEST)
        bytes.append(OUTGOING_REQUEST_INPUT_VALUES)
        for b in self.eol:
            bytes.append(b)

        #self.write(bytes, toConsole=False)
        self.start_action(SerAction.WAITING_ON_INPUT_VALUES, bytes, toConsole=False)

    def update(self):
        
        if self.action == SerAction.WAITING_ON_HANDSHAKE and self.timeSinceAction() > 1:
            print("WAITING...")
            #self.write(self.handshakeRequest)

            self.start_action(SerAction.WAITING_ON_HANDSHAKE, self.handshakeRequest)

        elif self.action == SerAction.WAITING_ON_DEVICE_CONFIG and self.timeSinceAction() > 1:
            print("WAITING...")
            #self.write(self.lastSent)
            self.start_action(SerAction.WAITING_ON_DEVICE_CONFIG, self.lastSent)

        elif self.action == SerAction.WAITING_ON_INPUT_CONFIG and self.timeSinceAction() > 1:
            print("WAITING...")
            #self.write(self.lastSent)
            self.start_action(SerAction.WAITING_ON_INPUT_CONFIG, self.lastSent)

        elif self.action == SerAction.WAITING_ON_INPUT_ALL_CONFIG and self.timeSinceAction() > 1:
            print("WAITING...")
            #self.write(self.lastSent)
            self.start_action(SerAction.WAITING_ON_INPUT_ALL_CONFIG, self.lastSent)

        elif self.action == SerAction.WAITING_ON_CONFIG_SEND_UPDATE_CONFIRMATION and self.timeSinceAction() > 1:
            print("WAITING...")
            #self.write(self.lastSent)
            self.start_action(SerAction.WAITING_ON_CONFIG_SEND_UPDATE_CONFIRMATION, self.lastSent)

        elif self.action == SerAction.WAITING_ON_CONFIG_SEND_UPDATE_CONFIRMATION_ALL and self.timeSinceAction() > 1:
            print("WAITING...")
            #self.write(self.lastSent)
            self.start_action(SerAction.WAITING_ON_CONFIG_SEND_UPDATE_CONFIRMATION_ALL, self.lastSent)
        
        elif self.action == SerAction.WAITING_ON_INPUT_VALUES and self.timeSinceAction() > 1:
            print("WAITING...")
            #self.write(self.lastSent)
            self.start_action(SerAction.WAITING_ON_INPUT_VALUES, self.lastSent, toConsole=False)
        
        elif self.action == SerAction.WAITING_ON_SAVE_TO_FLASH and self.timeSinceAction() > 1:
            print("WAITING...")
            #self.write(self.lastSent)
            self.start_action(SerAction.WAITING_ON_SAVE_TO_FLASH, self.lastSent, toConsole=False)
        
        
        elif self.action == SerAction.CONNECTED and self.timeSinceAction() > INPUT_VALUE_UPDATE_DELAY:
            if self.updateInputValues:
                self.requestInputValues()
            
        #print("im updating")

    def write(self, message, toConsole=True):
        #print("Sending: " + str(message))
        if message == None:
            return
        if toConsole:
            print("SEND")
            for b in range(len(message)):
                print(str(b) + ": " + str(message[b]))
        self.serial.write(message)

    def timeSinceAction(self):
        return time.time() - self.actionStartTime

    def try_connect(self, port):
        self.port = port
        self.serial = QtSerialPort.QSerialPort(
            port,
            baudRate=115200,
            readyRead=self.receive
        )
        self.serial.open(QIODevice.ReadWrite)
        
        #self.write(self.handshakeRequest)
        self.start_action(SerAction.WAITING_ON_HANDSHAKE, self.handshakeRequest)
        self.handshake_tries = 0

    def receive(self):
        self.char_buffer += self.serial.readAll()

        if self.char_buffer.contains(self.eol):
            idx = self.char_buffer.indexOf(self.eol)
            data = self.char_buffer[0: idx]
            data = data.data()
            #print("Received: " + str(data))
            #self.print_bytes(self.char_buffer)

            if data[0] == INCOMING_RESPONSE_INPUT_VALUES:
                self.start_action(SerAction.CONNECTED)
                #print("GOT VALUES")
                #self.print_bytes(data)
                self.parse_response_input_values(data)
            
            elif self.char_buffer.contains(self.handshakeResponse):
                self.start_action(SerAction.CONNECTED)
                self.master.SetUIPage(constant.PAGE_CONNECTED)

                self.request_device_config()

            elif data[0] == INCOMING_RESPONSE_DEVICE_CONFIG:
                self.start_action(SerAction.CONNECTED)
                self.print_bytes(data)
                print("RECEIVED RESPONSE")
                self.master.device = device.Device()
                self.master.device.SetFromConfigPacket(data)

                self.request_device_all_inputs_config()

            elif data[0] == INCOMING_RESPONSE_INPUT_CONFIG:
                self.start_action(SerAction.CONNECTED)
                print("RECEIVED RESPONSE")

            elif data[0] == INCOMING_RESPONSE_DELETE_INPUT_UPDATE:
                self.start_action(SerAction.CONNECTED)
                print("RECEIVED DELETE RESPONSE")

            elif data[0] == INCOMING_RESPONSE_SAVE_TO_FLASH:
                self.start_action(SerAction.CONNECTED)
                print("RECEIVED RESPONSE")
                print("Saved to NVM")     
                self.print_bytes(data)     

            elif data[0] == INCOMING_RESPONSE_INPUT_ALL_CONFIG:
                self.start_action(SerAction.CONNECTED)
                print("RECEIVED RESPONSE")
                self.parse_response_all_inputs_config(data)
                    
            elif data[0] == INCOMING_RESPONSE_RECEIVED_INPUT_CONFIG_UPDATE:                
                print("RECEIVED RESPONSE")
                # Continue sending next update.
                if self.action == SerAction.WAITING_ON_CONFIG_SEND_UPDATE_CONFIRMATION_ALL:
                    lastInputSent = data[1]
                    if lastInputSent < len(self.master.device.inputs)-1:
                        self.sendDeviceUpdate(lastInputSent+1, True)
                    else:
                        self.start_action(SerAction.CONNECTED)
                else:
                    self.start_action(SerAction.CONNECTED)
                    
                
                

            self.char_buffer.clear()

    def print_bytes(self, bytes):
        for i in range(len(bytes)):
            print(str(i) + ": " + str(bytes[i]))

    def request_device_config(self):
        request = bytearray()
        request.append(OUTGOING_REQUEST)
        request.append(OUTGOING_REQUEST_DEVICE_CONFIG)
        request.append(ord('\r'))
        request.append(ord('\n'))

        #self.write(request)        
        self.start_action(SerAction.WAITING_ON_DEVICE_CONFIG, request)

    def request_device_input_config(self, inputIndex):
        request = bytearray()
        request.append(OUTGOING_REQUEST)
        request.append(OUTGOING_REQUEST_INPUT_CONFIG)
        request.append(inputIndex)
        request.append(ord('\r'))
        request.append(ord('\n'))

        #self.write(request)        
        self.start_action(SerAction.WAITING_ON_INPUT_CONFIG, request)

    def request_device_all_inputs_config(self):
        request = bytearray()
        request.append(OUTGOING_REQUEST)
        request.append(OUTGOING_REQUEST_INPUT_ALL_CONFIG)
        request.append(ord('\r'))
        request.append(ord('\n'))

        #self.write(request)        
        self.start_action(SerAction.WAITING_ON_INPUT_ALL_CONFIG, request)


    def start_action(self, action, message=None, toConsole=True):
        self.write(message, toConsole)
        self.lastSent = message
        self.action = action
        self.actionStartTime = time.time()
        #print("Started Serial Action: " + str(action))

    def parse_response_input_values(self, data):
        inputCount = data[1]
        for i in range(inputCount):
            rawVal = data[2+i*4] << 8
            rawVal += data[3+i*4]  
            val = data[4+i*4] << 8
            val += data[5+i*4]  
            self.master.inputListPage.UpdateValues(i, rawVal, val)

    def parse_response_all_inputs_config(self, data):
        # 0 = INCOMING_RESPONSE_INPUT_ALL_CONFIG
        # 1 = input count
        # 21 bytes of config data * input count
        print("Received Full Config Data")
        print(str(data[1]) + " inputs")
        correctByteCount = 20 * data[1] + 2
        # if len(data) != correctByteCount:
        #     print("incorrect byte count")
        #     self.request_device_all_inputs_config()
        #     return
        
        self.print_bytes(data)
        inputCount = int((len(data) - 2) / 20)
        #print(inputCount)
        for i in range(0, inputCount * 20, 20):
            print(i)
            self.master.device.AddInput(data[i+2: i+20+2])

        self.master.OnDeviceConnected()
        self.master.device.SetInputLock(False)



def to_string(packet_bytes):
    count = 0
    for b in packet_bytes:
        print(str(count) + ": " + str(b))
        count += 1

def to_byte_array(string):
    b = bytearray()
    for i in string:
        b.append(ord(i))

    return b
        


def get_serial_ports():
    result = []
    for port in comports():
        result.append(str(port))
    return result

