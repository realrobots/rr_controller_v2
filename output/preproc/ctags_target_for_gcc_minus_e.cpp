# 1 "e:\\Documents\\Arduino\\rr_controller_v2\\rr_controller_v2\\rr_controller_v2.ino"
# 2 "e:\\Documents\\Arduino\\rr_controller_v2\\rr_controller_v2\\rr_controller_v2.ino" 2


// To switch between microcontroller types
// define only that one below
// and assign to to DEVICE_TYPE

// #define DUMMY
// #define ESP32 0
// #define ESP32S2 1
// #define ESP32S3 2





// #define USBMODE
//#define BLEMODE
//#define DUMMYMODE







# 28 "e:\\Documents\\Arduino\\rr_controller_v2\\rr_controller_v2\\rr_controller_v2.ino" 2
# 36 "e:\\Documents\\Arduino\\rr_controller_v2\\rr_controller_v2\\rr_controller_v2.ino"
long lastLoop;
long loopInterval = 1000 / 100;

void setup()
{
  // put your setup code here, to run once:

  pinMode(2, OUTPUT);
  for (int i = 0; i < 4; i++)
  {
    digitalWrite(2, HIGH);
    delay(50);
    digitalWrite(2, LOW);
    delay(50);
  }

  InitComms();
  InitNVM();
  // pinMode(15, OUTPUT);
  // digitalWrite(15, 1);
  InitInputs();
  InitHID();



}

void loop()
{
  // put your main code here, to run repeatedly:
  if (millis() - lastLoop > loopInterval)
  {
    UpdateInputs();
    lastLoop = millis();
  }

  CheckComms();

}
# 1 "e:\\Documents\\Arduino\\rr_controller_v2\\rr_controller_v2\\Comms.ino"
# 33 "e:\\Documents\\Arduino\\rr_controller_v2\\rr_controller_v2\\Comms.ino"
bool connectedToPC = false;
static char data[400];
static int serial_count;
static int c;

char b = 'E';


uint8_t zero = 0; // need this to insert 0 into Serial.write(0);

void InitComms()
{
    Serial.begin(115200);
    delay(400);
}

void CheckComms()
{
    while (Serial.available() > 0)
    {
        c = Serial.read();
        if (serial_count < 400)
        {
            data[serial_count] = c;
            serial_count++;
        }
    }

    // Incoming packet should be at least one byte plus the EOL \r\n
    if (serial_count > 2)
    {
        // Serial.println(serial_count);
        if (data[serial_count - 2] == '\r' && data[serial_count - 1] == '\n')
        {
            InterpretData();
            // Serial.println("Received");
        }
        ClearData();
    }
    else
    {
        ClearData();
    }
}

void InterpretData()
{
    if (data[0] == '\r' || data[0] == '\n')
    {
        return;
    }

    // if (data[0] == 'H' && data[1] == 'E')
    // {
    // }

    else if (data[0] == 36 /* '$'*/)
    {
        switch (data[1])
        {
        case 72 /* 'H'*/:
            Serial.write(37);
            Serial.write(72 /* 'H'*/);
            // Serial.println("Hello yourself!");
            Serial.write('\r');
            Serial.write('\n');
            break;
        case 110:
            Serial.write(37);
            Serial.write(111);
            Serial.write(GetAssignedInputCount());
            ReportInputValues();
            Serial.write('\r');
            Serial.write('\n');
            break;
        case 100:
            Serial.write(37);
            Serial.write(101);
            Serial.write((uint8_t)3);
            Serial.write((uint8_t)2);
            Serial.write(GetAssignedInputCount());
            Serial.write(GetMacroCount());
            Serial.write(zero); // spare
            Serial.write(zero); // spare
            Serial.write(zero); // spare
            Serial.write('\r');
            Serial.write('\n');
            break;
        case 102:
            Serial.write(37);
            // Byte #2 contains requested input index
            // Serial.print(data[2]);
            Serial.write(103);
            Serial.write(data[2]);
            ReportInputConfig(data[2]);
            Serial.write('\r');
            Serial.write('\n');
            break;
        case 114:
            Serial.write(37);
            Serial.write(115);
            Serial.write(data[2]);
            ReportMacroConfig(data[2]);
            Serial.write('\r');
            Serial.write('\n');
            break;
        case 104:
            Serial.write(37);
            // Byte #2 contains requested input index
            // Serial.print(data[2]);
            Serial.write(105);
            // Serial.write(0);
            Serial.write(GetAssignedInputCount());
            for (int i = 0; i < GetAssignedInputCount(); i++)
            {
                ReportInputConfig(i);
            }
            Serial.write('\r');
            Serial.write('\n');
            break;
        case 106:
            Serial.write(37);
            // data[2] == input index

            UpdateInputConfig(data);
            Serial.write(107);

            Serial.write(data[2]);
            Serial.write('o');
            Serial.write('k');

            Serial.write('\r');
            Serial.write('\n');

            break;
        case 108:
            Serial.write(37);
            DeleteInput(data[2]);
            Serial.write(109);

            Serial.write(data[2]);
            Serial.write('o');
            Serial.write('k');

            Serial.write('\r');
            Serial.write('\n');
            break;


        case 122:
            Serial.write(37);
            DeleteMacro(data[2]);
            Serial.write(123);

            Serial.write(data[2]);
            Serial.write('o');
            Serial.write('k');

            Serial.write('\r');
            Serial.write('\n');
            break;

        case 124:
            Serial.write(37);
            DeleteMacroBinding(data[2], data[3]);
            Serial.write(125);

            Serial.write(data[2]);
            Serial.write(data[3]);
            Serial.write('o');
            Serial.write('k');

            Serial.write('\r');
            Serial.write('\n');
            break;

        case 120:
            Serial.write(37);
            Serial.write(121);
            Serial.write(data[2]);
            AddMacro(data);
            Serial.write('o');
            Serial.write('k');
            Serial.write('\r');
            Serial.write('\n');

            break;

        case 118:
            Serial.write(37);
            Serial.write(119);
            SetMacroName(data);
            Serial.write('o');
            Serial.write('k');
            Serial.write('\r');
            Serial.write('\n');
            break;
        case 116:

            UpdateMacroBindingConfig(data);

            Serial.write(37);
            Serial.write(117);
            Serial.write(data[2]);
            Serial.write('o');
            Serial.write('k');
            Serial.write('\r');
            Serial.write('\n');
            break;
        case 112:
            Serial.write(37);
            Serial.write(113);
            SaveAllToNVM();
            Serial.write('o');
            Serial.write('k');

            Serial.write('\r');
            Serial.write('\n');

            break;
        default:
            Serial.write(37);
            Serial.println("UNKNOWN MESSAGE");
        }
    }
}

void ClearData()
{
    for (int i = 0; i < 400; i++)
    {
        data[i] = 0;
    }
    serial_count = 0;
}
# 1 "e:\\Documents\\Arduino\\rr_controller_v2\\rr_controller_v2\\Configuration.ino"
uint8_t HIDMode = 0;

char deviceName[] = "NEW_DEVICE      "; //16 chars


uint8_t GetHIDMode(){
  return HIDMode;
}
# 1 "e:\\Documents\\Arduino\\rr_controller_v2\\rr_controller_v2\\Inputs.ino"
// #define NONE     0
// #define GAMEPAD  1
// #define KEYBOARD 2
// #define MOUSE    3



Input inputs[64];
ButtonMacro macros[16];
HIDInterface gamepad;
uint8_t i2c_address = 0;
# 22 "e:\\Documents\\Arduino\\rr_controller_v2\\rr_controller_v2\\Inputs.ino"
void InitInputs()
{

    uint8_t firstRun = Read8BitValue(4095/*1281*/);
    Serial.println(firstRun);

    // Loads PWM_MAX values to inputs as defined for each microcontroller
    for (int i = 0; i < 64; i++)
    {
        inputs[i].SetPWM_MAX(1023);
    }

    if (firstRun == 255)
    {
        OnFirstRun();
    }
    else
    {
        Serial.println("LOADING CONFIG FROM NVM");
        LoadConfigFromNVM();

        Serial.println("LOADING INPUTS FROM NVM");
        LoadInputsFromNVM();

        Serial.println("LOADING MACROS FROM NVM");
        LoadMacrosFromNVM();
        delay(500);
        Serial.println(GetMacroCount());
        Serial.println("HELLO");
    }

    // for (int i = 0; i < 4; i++){
    //     Serial.print(i);
    //     Serial.print("\t");
    //     inputs[i].GetBinding().PrintMe();
    // }

    // macros[0].AddBinding(Binding(GAMEPAD, GAMEPAD_BUTTON, 1, 1));
    // macros[0].AddBinding(Binding(GAMEPAD, GAMEPAD_BUTTON, 1, 0));
    //  macros[0].AddBinding(Binding(GAMEPAD, GAMEPAD_BUTTON, 2, 1));
    //  macros[0].AddBinding(Binding(GAMEPAD, GAMEPAD_BUTTON, 2, 0));
    //  macros[0].AddBinding(Binding(GAMEPAD, GAMEPAD_BUTTON, 3, 1));
    //  macros[0].AddBinding(Binding(GAMEPAD, GAMEPAD_BUTTON, 3, 0));
    //  macros[0].AddBinding(Binding(GAMEPAD, GAMEPAD_BUTTON, 4, 1));
    //  macros[0].AddBinding(Binding(GAMEPAD, GAMEPAD_BUTTON, 4, 0));
    //  macros[0].AddBinding(Binding(GAMEPAD, GAMEPAD_BUTTON, 5, 1));
    //  macros[0].AddBinding(Binding(GAMEPAD, GAMEPAD_BUTTON, 5, 0));

    // macros[1].AddBinding(Binding(GAMEPAD, GAMEPAD_HAT, 1, 1));
    // macros[1].AddBinding(Binding(GAMEPAD, GAMEPAD_HAT, 1, 0));
    // macros[1].AddBinding(Binding(GAMEPAD, GAMEPAD_HAT, 2, 1));
    // macros[1].AddBinding(Binding(GAMEPAD, GAMEPAD_HAT, 2, 0));
    // macros[1].AddBinding(Binding(GAMEPAD, GAMEPAD_HAT, 3, 1));
    // macros[1].AddBinding(Binding(GAMEPAD, GAMEPAD_HAT, 3, 0));
    // macros[1].AddBinding(Binding(GAMEPAD, GAMEPAD_HAT, 4, 1));
    // macros[1].AddBinding(Binding(GAMEPAD, GAMEPAD_HAT, 4, 0));

    if (false)
    {

        inputs[0].SetBinding(Binding(1, 3 /* 3 hats, 8 directions each, assignedInput = dir + hatIdx*9*/, 1, 0, 4));
        inputs[0].SetPinMode(INPUT_PULLUP);
        inputs[0].SetPin(27);
        inputs[0].SetIsAnalog(true);
        inputs[0].SetIsInverted(false);
        inputs[0].SetFilteringSamples(8);
        inputs[0].SetDeadZone(726);
        inputs[0].InitInput();

        inputs[1].SetBinding(Binding(1, 3 /* 3 hats, 8 directions each, assignedInput = dir + hatIdx*9*/, 2, 0, 4));
        inputs[1].SetPinMode(INPUT);
        inputs[1].SetPin(35);
        inputs[1].SetIsAnalog(true);
        inputs[1].SetIsInverted(true);
        inputs[1].SetFilteringSamples(3);
        inputs[1].InitInput();

        inputs[2].SetBinding(Binding(1, 3 /* 3 hats, 8 directions each, assignedInput = dir + hatIdx*9*/, 3, 0, 4));
        inputs[2].SetPinMode(INPUT);
        inputs[2].SetPin(39);
        inputs[2].SetIsAnalog(true);
        inputs[2].SetIsInverted(true);
        inputs[2].SetFilteringSamples(16);
        inputs[2].InitInput();

        // int testPins[] = {32, 33, 25, 26, 27, 14, 12, 13, 15, 2, 4, 16};

        // for (int i = 3; i < 3 + 12; i++)
        // {

        //     inputs[i].SetBinding(Binding(GAMEPAD, GAMEPAD_BUTTON, i - 2));
        //     inputs[i].SetPinMode(INPUT_PULLUP);
        //     inputs[i].SetPin(testPins[i - 3]);
        //     inputs[i].SetIsAnalog(false);
        //     inputs[i].SetIsInverted(true);
        //     inputs[i].InitInput();
        // }

        // int hatPins[] = {17, 18, 19, 21};

        // for (int i = 0; i < 4; i++)
        // {
        //     inputs[15 + i].SetBinding(Binding(GAMEPAD, GAMEPAD_HAT, 1 + i * 2, 0, CONSTANT));
        //     inputs[15 + i].SetPinMode(INPUT_PULLUP);
        //     inputs[15 + i].SetPin(hatPins[i]);
        //     inputs[15 + i].SetIsAnalog(false);
        //     inputs[15 + i].SetIsInverted(true);
        //     inputs[15 + i].InitInput();
        // }
    }
}

void InitHID()
{
    gamepad = HIDInterface();
    gamepad.InitHID();
}

uint8_t GetAssignedInputCount()
{
    for (int i = 0; i < 64; i++)
    {
        if (inputs[i].GetPin() == 255)
        {
            return i;
        }
    }
    return 0;
}

uint8_t GetMacroCount()
{
    for (int i = 0; i < 16; i++)
    {
        if (macros[i].GetBindingCount() == 0)
        {
            return i;
        }
    }
    return 0;
}

void UpdateInputs()
{
    for (int i = 0; i < 64; i++)
    {
        inputs[i].UpdateState();
    }

    for (int i = 0; i < 64; i++)
    {
        // Serial.print(i);
        // Serial.print("\t");
        // Serial.println(inputs[i].GetBinding().GetTrigger());
        if (inputs[i].GetBinding().GetTrigger() == 4)
        {
            // if (i == 1)
            // {
            //     Serial.println(inputs[i].GetBinding().GetDeviceType());
            // }
            SendInput(inputs[i].GetBinding());
        }
        else
        {
            switch (inputs[i].GetBinding().GetState())
            {
            case 0:

                break;
            case 2:
                if (inputs[i].GetBinding().GetTrigger() == 2)
                {
                    //Serial.println(i);
                    SendInput(inputs[i].GetBinding());
                }
                break;
            case 1:

                break;
            case 3:
                if (inputs[i].GetBinding().GetTrigger() == 3)
                {
                    SendInput(inputs[i].GetBinding());
                }
                break;
            }
        }
    }

    for (int i = 0; i < 16; i++)
    {
        if (macros[i].IsValid())
        {
            if (macros[i].IsActive())
            {
                Binding b = macros[i].GetNextBinding();
                if (b.GetDeviceType() != 0)
                {
                    SendInput(b);
                }
            }
        }
    }

    gamepad.SendReport();
}

void SendInput(Binding binding)
{
    // Serial.println("SendInput()");
    // Serial.println(binding.GetDeviceType());
    switch (binding.GetDeviceType())
    {
    case 0:

        break;
    case 1:
        // Temp fix which fits macros in under gamepad
        //(should be own device type but device types not implemented yet)
        if (binding.GetInputType() == 4)
        {
            macros[binding.GetAssignedInput()].StartMacro();
        }
        else
        {
            SendGamepadInput(binding);
        }
        break;
    case 2:

        break;
    case 3:

        break;
    case 4:
        // Serial.println("MACRO");
        if (binding.GetVal() == 1)
        {
            // Serial.println("START MACRO");
            macros[binding.GetAssignedInput()].StartMacro();
        }
        break;
    }
}

void SendGamepadInput(Binding binding)
{
    // Serial.println(binding.GetInputType());
    switch (binding.GetInputType())
    {
    case 1 /* 1-128*/:
        gamepad.SetGamepadButton(binding.GetAssignedInput(),
                                 binding.GetVal());
        break;
    case 2 /* 0-7  x, y, z, rZ, rX, rY, slider1, slider2*/:
        gamepad.SetGamepadAxis(binding.GetAssignedInput(),
                               binding.GetVal());
        break;
    case 3 /* 3 hats, 8 directions each, assignedInput = dir + hatIdx*9*/:
        if (binding.GetVal() == 1)
        {
            gamepad.SetGamepadHat(binding.GetAssignedInput());
        }
        break;
    case 4 /* 0=Start, 1=Select, 2=Menu, 3=Home, 4=Back, 5=Volume_inc, 6=Volume_dec, 7=Volume_mute*/:
        gamepad.SetGamepadSpecial(binding.GetAssignedInput(),
                                  binding.GetVal());
        break;
    }
}

void ReportInputConfig(uint8_t index)
{
    if (index >= 0 && index < 64)
    {
        inputs[index].ReportConfig();
    }
}

void AddMacro(char *data)
{
    if (data[2] >= 0 && data[2] <= 16)
    {
        macros[data[2]].NewMacro(data);
    }
}

void SetMacroName(char *data)
{

    if (data[2] >= 0 && data[2] <= 16)
    {
        macros[data[2]].SetMacroName(data);
    }
}

void ReportMacroConfig(uint8_t index)
{
    if (index >= 0 && index < 16)
    {
        macros[index].ReportConfig();
    }
}

void ReportInputValues()
{
    for (int i = 0; i < GetAssignedInputCount(); i++)
    {
        inputs[i].ReportInputValues();
    }
}

void UpdateInputConfig(char *data)
{
    inputs[data[2]].UpdateInputConfig(data);
}

void DeleteInput(uint8_t idx)
{
    for (int i = idx; i < GetAssignedInputCount(); i++)
    {
        inputs[i] = inputs[i + 1];
    }
    inputs[GetAssignedInputCount()].SetToDefaults();
}

void DeleteMacro(uint8_t idx)
{
    for (int i = idx; i < GetMacroCount(); i++)
    {
        macros[i] = macros[i + 1];
    }
    macros[GetMacroCount()].SetToDefaults();
}

void DeleteMacroBinding(uint8_t macroIdx, uint8_t bindingIdx)
{
    macros[macroIdx].DeleteBinding(bindingIdx);
}

void UpdateMacroBindingConfig(char *data)
{
    macros[data[2]].SetBindingConfig(data);
}

void LoadConfigFromNVM()
{
    // Load deviceName from EEPROM
    for (int i = 0; i < 16; i++)
    {
        deviceName[i] = char(Read8BitValue(1282 /* 16 chars*/ + i));
    }

    // Load i2cAddress from EEPROM
    i2c_address = Read8BitValue(1299);
}

void LoadInputsFromNVM()
{
    uint8_t inputCount = Read8BitValue(1300);
    char data[32];
    for (int i = 0; i < 32; i++)
    {
        data[i] = 0;
    }
    for (int i = 0; i < inputCount; i++)
    {
        for (int b = 0; b < 23; b++)
        {
            data[b] = Read8BitValue(0 + i * 20 + b);
            // Serial.print(b);
            // Serial.print(' ');
            // Serial.println(int(data[b]));
        }
        ////  Serial.println();

        inputs[i].UpdateInputConfig(data);
    }
}

void LoadMacrosFromNVM()
{
    uint8_t macroCount = Read8BitValue(1301);
    int currentByte = 1302;
    for (int i = 0; i < macroCount; i++)
    {
        for (int n = 0; n < 16; n++)
        {
            macros[i].macroName[n] = Read8BitValue(currentByte + n);
        }
        currentByte += 16;
        // macros[i].AddBinding(Binding());

        uint8_t bindingCount = Read8BitValue(currentByte);
        Serial.print("Binding count: ");
        Serial.println(bindingCount);
        currentByte += 1;
        char data[11];
        for (uint8_t z = 0; z < bindingCount; z++)
        {
            data[2] = i;
            data[3] = z;
            for (uint8_t b = 4; b < 11; b++)
            {
                data[b] = Read8BitValue(currentByte);
                currentByte += 1;
            }
            macros[i].AddBinding(Binding());
            macros[i].SetBindingConfig(data);
        }
    }
}

void SaveAllToNVM()
{
    // Save deviceName to NVM
    for (int i = 0; i < 16; i++)
    {
        Store8BitValue(1282 /* 16 chars*/ + i, deviceName[i]);
    }

    // Save i2cAddress to NVM
    Store8BitValue(1299, i2c_address);

    Store8BitValue(1300, GetAssignedInputCount());
    Serial.write(GetAssignedInputCount());
    for (int i = 0; i < GetAssignedInputCount() + 1; i++)
    {
        Store8BitValue(0 + i * 20 + 3, inputs[i].GetPin());
        Store8BitValue(0 + i * 20 + 4, inputs[i].GetPinMode());
        Store8BitValue(0 + i * 20 + 5, inputs[i].IsAnalog());
        Store8BitValue(0 + i * 20 + 6, inputs[i].GetIsInverted());

        Store8BitValue(0 + i * 20 + 7, ((uint8_t) ((inputs[i].GetMinVal()) >> 8)));
        Store8BitValue(0 + i * 20 + 8, ((uint8_t) ((inputs[i].GetMinVal()) & 0xff)));
        Store8BitValue(0 + i * 20 + 9, ((uint8_t) ((inputs[i].GetMidVal()) >> 8)));
        Store8BitValue(0 + i * 20 + 10, ((uint8_t) ((inputs[i].GetMidVal()) & 0xff)));
        Store8BitValue(0 + i * 20 + 11, ((uint8_t) ((inputs[i].GetMaxVal()) >> 8)));
        Store8BitValue(0 + i * 20 + 12, ((uint8_t) ((inputs[i].GetMaxVal()) & 0xff)));

        Store8BitValue(0 + i * 20 + 13, ((uint8_t) ((inputs[i].GetDeadZone()) >> 8)));
        Store8BitValue(0 + i * 20 + 14, ((uint8_t) ((inputs[i].GetDeadZone()) & 0xff)));
        Store8BitValue(0 + i * 20 + 15, inputs[i].GetBufferSize());

        Store8BitValue(0 + i * 20 + 16, inputs[i].GetBinding().GetDeviceType());
        Store8BitValue(0 + i * 20 + 17, inputs[i].GetBinding().GetInputType());
        Store8BitValue(0 + i * 20 + 18, inputs[i].GetBinding().GetAssignedInput());

        Store8BitValue(0 + i * 20 + 19, ((uint8_t) ((inputs[i].GetBinding().GetVal()) >> 8)));

        Store8BitValue(0 + i * 20 + 20, ((uint8_t) ((inputs[i].GetBinding().GetVal()) & 0xff)));

        Store8BitValue(0 + i * 20 + 22, inputs[i].GetBinding().GetTrigger());
    }

    Store8BitValue(1301, GetMacroCount());
    Serial.write(123);
    Serial.write(GetMacroCount());
    int currentByte = 1302;
    for (int i = 0; i < GetMacroCount(); i++)
    {
        for (int n = 0; n < 16; n++)
        {
            Store8BitValue(currentByte + n, macros[i].macroName[n]);
        }
        currentByte += 16;

        uint8_t bindingCount = macros[i].GetBindingCount();
        Store8BitValue(currentByte, bindingCount);
        currentByte++;

        for (int b = 0; b < bindingCount; b++)
        {
            Store8BitValue(currentByte + 0, macros[i].GetBinding(b).GetDeviceType());
            Store8BitValue(currentByte + 1, macros[i].GetBinding(b).GetInputType());
            Store8BitValue(currentByte + 2, macros[i].GetBinding(b).GetAssignedInput());
            Store8BitValue(currentByte + 3, ((uint8_t) ((macros[i].GetBinding(b).GetVal()) >> 8)));
            Store8BitValue(currentByte + 4, ((uint8_t) ((macros[i].GetBinding(b).GetVal()) & 0xff)));
            Store8BitValue(currentByte + 5, macros[i].GetBinding(b).GetState());
            Store8BitValue(currentByte + 6, macros[i].GetBinding(b).GetTrigger());
            currentByte += 7;
        }
    }
}

void OnFirstRun()
{
    Serial.println("First Run");
    char defaultName[] = "NEW_DEVICE      ";
    for (int i = 0; i < 16; i++)
    {
        Store8BitValue(1282 /* 16 chars*/ + i, defaultName[i]);
    }

    // Write default i2c_address to EEPROM
    Store8BitValue(1299, 0);

    Store8BitValue(4095/*1281*/, 1); // mark first run byte

    Store8BitValue(1300, 0);

    Store8BitValue(1301, 0);
}
# 1 "e:\\Documents\\Arduino\\rr_controller_v2\\rr_controller_v2\\NonVolatileStorage.ino"
// Non-volatile memory interface
// Accesses EEPROM for Atmega32U4 devices and preferences for ESP32
// Limited by Atmega32U4 1024 bytes of EEPROM memory

// 0 - 20      inputs[0]
// 0 - 1280 (20*64)    inputs[*]
// 800 - 847   ENCODER_ASSIGNEMENTS (48)
// 1000 - 1015 DEVICE_NAME
// 1022        I2C_ADDRESS
// 1024        FIRSTRUN (100 == false)

// ATMEGA 2560
// 0 - 14      inputs[0]
// 0 - 882     inputs[*]
// 2012 - 2043   MATRIX_PIN_ASSIGNMENTS (32)
// 2044 - 2299   MATRIX_BUTTON_ASSIGNMENTS (256)
// 2300 - 2347   ENCODER_ASSIGNEMENTS (48)
// 4000 - 4015 DEVICE_NAME
// 4022        I2C_ADDRESS
// 4024        FIRSTRUN (100 == false)
# 22 "e:\\Documents\\Arduino\\rr_controller_v2\\rr_controller_v2\\NonVolatileStorage.ino" 2


void InitNVM()
{
    EEPROM.begin(4096);

}

void Store8BitValue(uint16_t idx, uint8_t val)
{
    EEPROM.write(idx, val);
    EEPROM.commit();
}

uint8_t Read8BitValue(uint16_t idx)
{
    return EEPROM.read(idx);
}
