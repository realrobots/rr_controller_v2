#line 1 "e:\\Documents\\Arduino\\rr_controller_v2\\rr_controller_v2\\USBHIDInterface.h"
#include <USB.h>
#include <USBHIDGamepad.h>

#define numOfButtons 128
#define numOfHatSwitches 4

#define enableX false
#define enableY false
#define enableZ true
#define enableRX true
#define enableRY true
#define enableRZ false
#define enableSlider1 true
#define enableSlider2 false

#define enableSteering true    // X
#define enableAccelerator true // Y
#define enableRudder false     // RZ / RUDDER
#define enableBrake true       // RZ / RUDDER
#define enableThrottle true    // SLIDER 2

// USBHID ONLY
int8_t x;
int8_t y;
int8_t z;
int8_t rz;
int8_t rx;
int8_t ry;
uint8_t hat;
uint32_t buttons;

class HIDInterface
{

private:
    // BleGamepad bleGamepad;
    // BleGamepadConfiguration bleGamepadConfig;
    USBHIDGamepad Gamepad;

    std::string btName = "RealRobots HID Device";
    std::string btManufacturer = "RealRobots";

    uint8_t hats[4] = {};

public:
    void InitHID()
    {
        Serial.println("Starting USBHID work!");
        // bleGamepad = BleGamepad(btName, btManufacturer, 100);
        // bleGamepadConfig.setAutoReport(false);
        // bleGamepadConfig.setControllerType(CONTROLLER_TYPE_GAMEPAD); // CONTROLLER_TYPE_JOYSTICK, CONTROLLER_TYPE_GAMEPAD (DEFAULT), CONTROLLER_TYPE_MULTI_AXIS
        // bleGamepadConfig.setButtonCount(numOfButtons);
        // bleGamepadConfig.setHatSwitchCount(numOfHatSwitches);
        // bleGamepadConfig.setWhichSpecialButtons(true, true, true, true, true, true, true, true);
        // bleGamepadConfig.setVid(0xe502);
        // bleGamepadConfig.setPid(0xabcd);
        // bleGamepadConfig.setAxesMin(0x0000); // 0 --> int16_t - 16 bit signed integer - Can be in decimal or hexadecimal
        // bleGamepadConfig.setAxesMax(0x7FFF); // 32767 --> int16_t - 16 bit signed integer - Can be in decimal or hexadecimal
        // bleGamepadConfig.setWhichAxes(enableX, enableY, enableZ, enableRX, enableRY, enableRZ, enableSlider1, enableSlider2);
        // bleGamepadConfig.setWhichSimulationControls(enableRudder, enableThrottle, enableAccelerator, enableBrake, enableSteering);

        // bleGamepad.begin(&bleGamepadConfig);
        Gamepad.begin();
        USB.begin();
    }

    void SetGamepadButton(uint8_t button, uint8_t state)
    {
        bitWrite(buttons, button, state);
    }

    // Special buttons not working, not sure why
    void SetGamepadSpecial(uint8_t button, uint8_t state)
    {
        if (state)
        {

            // Gamepad.pressSpecialButton(button);
        }
        else
        {
            // Gamepad.pressSpecialButton(button);
        }
    }

    // Hat number = hatIdx/9, hat position = hatIdx % 9
    void SetGamepadHat(uint8_t hatIdx)
    {
        hats[hatIdx / 9] = hatIdx % 9;
    }

    void SetGamepadAxis(uint8_t axis, int16_t value)
    {
        // Serial.print(axis);
        // Serial.print("\t");
        // Serial.println(value);
        switch (axis)
        {
        case 0: // X Axis
            x = value;
            break;
        case 1: // Y Axis
            y = value;
            break;
        case 2: // Z Axis
            z = value;
            break;
        case 3: // rX Axis
            rx = value;
            break;
        case 4: // rY Axis
            ry = value;
            break;
        case 5: // rZ Axis
            rx = value;
            break;
        case 6: // Slider1 Axis
            // Gamepad.setSlider1(value);
            break;
        case 7: // Slider2 Axis
            // Gamepad.setSlider2(value);
            break;
        case 8:
            // Gamepad.setSteering(value);
            break;
        case 9:
            // Gamepad.setAccelerator(value);
            break;
        case 10:
            // Gamepad.setRudder(value);
            break;
        case 11:
            // Gamepad.setBrake(value);
            break;
        case 12:
            // Gamepad.setThrottle(value);
            break;
        case 13:
            // Gamepad.setBatteryLevel(map(value, 0, 32767, 0, 100));
            break;
        }
    }

    void SendHats()
    {
        // Gamepad.setHats(hats[0], hats[1], hats[2], hats[3]);
        for (int i = 0; i < 1; i++)
        {
            hats[i] = 0;
        }
    }

    void SendReport()
    {
        Gamepad.send(x, y, z, rz, rx, ry, hats[0], buttons);
        SendHats();
    }
};