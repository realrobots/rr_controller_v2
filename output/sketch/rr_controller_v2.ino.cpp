#include <Arduino.h>
#line 1 "e:\\Documents\\Arduino\\rr_controller_v2\\rr_controller_v2\\rr_controller_v2.ino"
#include "InputClass.h"


// To switch between microcontroller types
// define only that one below
// and assign to to DEVICE_TYPE

// #define DUMMY
// #define ESP32 0
// #define ESP32S2 1
// #define ESP32S3 2
#define RP2040 3

#define FIRMWARE_VERSION 2
#define DEVICE_TYPE RP2040

// #define USBMODE
//#define BLEMODE
//#define DUMMYMODE

#ifdef ESP32
#include "BLEHIDInterface.h"
#define PWM_MAX 4095
#endif

#ifdef RP2040
#include "RP2040USBHIDInterface.h"
#define PWM_MAX 1023
#endif

#ifdef DUMMY
#include "DummyHIDInterface.h"
#define PWM_MAX 4095
#endif

long lastLoop;
long loopInterval = 1000 / 100;

#line 39 "e:\\Documents\\Arduino\\rr_controller_v2\\rr_controller_v2\\rr_controller_v2.ino"
void setup();
#line 63 "e:\\Documents\\Arduino\\rr_controller_v2\\rr_controller_v2\\rr_controller_v2.ino"
void loop();
#line 43 "e:\\Documents\\Arduino\\rr_controller_v2\\rr_controller_v2\\Comms.ino"
void InitComms();
#line 49 "e:\\Documents\\Arduino\\rr_controller_v2\\rr_controller_v2\\Comms.ino"
void CheckComms();
#line 78 "e:\\Documents\\Arduino\\rr_controller_v2\\rr_controller_v2\\Comms.ino"
void InterpretData();
#line 260 "e:\\Documents\\Arduino\\rr_controller_v2\\rr_controller_v2\\Comms.ino"
void ClearData();
#line 6 "e:\\Documents\\Arduino\\rr_controller_v2\\rr_controller_v2\\Configuration.ino"
uint8_t GetHIDMode();
#line 22 "e:\\Documents\\Arduino\\rr_controller_v2\\rr_controller_v2\\Inputs.ino"
void InitInputs();
#line 134 "e:\\Documents\\Arduino\\rr_controller_v2\\rr_controller_v2\\Inputs.ino"
void InitHID();
#line 140 "e:\\Documents\\Arduino\\rr_controller_v2\\rr_controller_v2\\Inputs.ino"
uint8_t GetAssignedInputCount();
#line 152 "e:\\Documents\\Arduino\\rr_controller_v2\\rr_controller_v2\\Inputs.ino"
uint8_t GetMacroCount();
#line 164 "e:\\Documents\\Arduino\\rr_controller_v2\\rr_controller_v2\\Inputs.ino"
void UpdateInputs();
#line 229 "e:\\Documents\\Arduino\\rr_controller_v2\\rr_controller_v2\\Inputs.ino"
void SendInput(Binding binding);
#line 267 "e:\\Documents\\Arduino\\rr_controller_v2\\rr_controller_v2\\Inputs.ino"
void SendGamepadInput(Binding binding);
#line 293 "e:\\Documents\\Arduino\\rr_controller_v2\\rr_controller_v2\\Inputs.ino"
void ReportInputConfig(uint8_t index);
#line 301 "e:\\Documents\\Arduino\\rr_controller_v2\\rr_controller_v2\\Inputs.ino"
void AddMacro(char *data);
#line 309 "e:\\Documents\\Arduino\\rr_controller_v2\\rr_controller_v2\\Inputs.ino"
void SetMacroName(char *data);
#line 318 "e:\\Documents\\Arduino\\rr_controller_v2\\rr_controller_v2\\Inputs.ino"
void ReportMacroConfig(uint8_t index);
#line 326 "e:\\Documents\\Arduino\\rr_controller_v2\\rr_controller_v2\\Inputs.ino"
void ReportInputValues();
#line 334 "e:\\Documents\\Arduino\\rr_controller_v2\\rr_controller_v2\\Inputs.ino"
void UpdateInputConfig(char *data);
#line 339 "e:\\Documents\\Arduino\\rr_controller_v2\\rr_controller_v2\\Inputs.ino"
void DeleteInput(uint8_t idx);
#line 348 "e:\\Documents\\Arduino\\rr_controller_v2\\rr_controller_v2\\Inputs.ino"
void DeleteMacro(uint8_t idx);
#line 357 "e:\\Documents\\Arduino\\rr_controller_v2\\rr_controller_v2\\Inputs.ino"
void DeleteMacroBinding(uint8_t macroIdx, uint8_t bindingIdx);
#line 362 "e:\\Documents\\Arduino\\rr_controller_v2\\rr_controller_v2\\Inputs.ino"
void UpdateMacroBindingConfig(char *data);
#line 367 "e:\\Documents\\Arduino\\rr_controller_v2\\rr_controller_v2\\Inputs.ino"
void LoadConfigFromNVM();
#line 379 "e:\\Documents\\Arduino\\rr_controller_v2\\rr_controller_v2\\Inputs.ino"
void LoadInputsFromNVM();
#line 402 "e:\\Documents\\Arduino\\rr_controller_v2\\rr_controller_v2\\Inputs.ino"
void LoadMacrosFromNVM();
#line 435 "e:\\Documents\\Arduino\\rr_controller_v2\\rr_controller_v2\\Inputs.ino"
void SaveAllToNVM();
#line 507 "e:\\Documents\\Arduino\\rr_controller_v2\\rr_controller_v2\\Inputs.ino"
void OnFirstRun();
#line 24 "e:\\Documents\\Arduino\\rr_controller_v2\\rr_controller_v2\\NonVolatileStorage.ino"
void InitNVM();
#line 30 "e:\\Documents\\Arduino\\rr_controller_v2\\rr_controller_v2\\NonVolatileStorage.ino"
void Store8BitValue(uint16_t idx, uint8_t val);
#line 36 "e:\\Documents\\Arduino\\rr_controller_v2\\rr_controller_v2\\NonVolatileStorage.ino"
uint8_t Read8BitValue(uint16_t idx);
#line 39 "e:\\Documents\\Arduino\\rr_controller_v2\\rr_controller_v2\\rr_controller_v2.ino"
void setup()
{
  // put your setup code here, to run once:

  pinMode(2, OUTPUT);
  for (int i = 0; i < 4; i++)
  {
    digitalWrite(2, HIGH);
    delay(50);
    digitalWrite(2, LOW);
    delay(50);
  }

  InitComms();
  InitNVM();
  // pinMode(15, OUTPUT);
  // digitalWrite(15, 1);
  InitInputs();
  InitHID();

  

}

void loop()
{
  // put your main code here, to run repeatedly:
  if (millis() - lastLoop > loopInterval)
  {
    UpdateInputs();
    lastLoop = millis();
  }

  CheckComms();

}

#line 1 "e:\\Documents\\Arduino\\rr_controller_v2\\rr_controller_v2\\Comms.ino"
#define COMMAND_SIZE 400

#define INCOMING_REQUEST 36 // '$'
#define RESPONSE_HEADER 37
#define INCOMING_HANDSHAKE_REQUEST 72 // 'H'
#define INCOMING_REQUEST_DEVICE_CONFIG 100
#define INCOMING_REQUEST_DEVICE_CONFIG_RESPONSE 101
#define INCOMING_REQUEST_INPUT_CONFIG 102
#define INCOMING_REQUEST_INPUT_CONFIG_RESPONSE 103
#define INCOMING_REQUEST_INPUT_ALL_CONFIG 104
#define INCOMING_REQUEST_INPUT_ALL_CONFIG_RESPONSE 105
#define INCOMING_RECEIVE_INPUT_CONFIG_UPDATE 106
#define INCOMING_RECEIVE_INPUT_CONFIG_UPDATE_RESPONSE 107
#define INCOMING_DELETE_INPUT_UPDATE 108
#define INCOMING_RESPONSE_DELETE_INPUT_UPDATE 109
#define INCOMING_REQUEST_INPUT_VALUES 110
#define INCOMING_RESPONSE_INPUT_VALUES 111
#define INCOMING_REQUEST_SAVE_TO_FLASH 112
#define INCOMING_RESPONSE_SAVE_TO_FLASH 113
#define INCOMING_REQUEST_MACRO_CONFIG 114
#define INCOMING_REQUEST_MACRO_CONFIG_RESPONSE 115
#define INCOMING_REQUEST_MACRO_BINDING_UPDATE 116
#define INCOMING_REQUEST_MACRO_BINDING_UPDATE_RESPONSE 117
#define INCOMING_REQUEST_MACRO_NAME_UPDATE 118
#define INCOMING_REQUEST_MACRO_NAME_UPDATE_RESPONSE 119
#define INCOMING_REQUEST_MACRO_CONFIG_UPDATE 120
#define INCOMING_REQUEST_MACRO_CONFIG_UPDATE_RESPONSE 121
#define INCOMING_REQUEST_DELETE_MACRO_UPDATE 122
#define INCOMING_REQUEST_DELETE_MACRO_UPDATE_RESPONSE 123
#define INCOMING_REQUEST_DELETE_MACRO_BINDING_UPDATE 124
#define INCOMING_REQUEST_DELETE_MACRO_BINDING_UPDATE_RESPONSE 125

bool connectedToPC = false;
static char data[COMMAND_SIZE];
static int serial_count;
static int c;

char b = 'E';


uint8_t zero = 0; // need this to insert 0 into Serial.write(0);

void InitComms()
{
    Serial.begin(115200);
    delay(400);
}

void CheckComms()
{
    while (Serial.available() > 0)
    {
        c = Serial.read();
        if (serial_count < COMMAND_SIZE)
        {
            data[serial_count] = c;
            serial_count++;
        }
    }

    // Incoming packet should be at least one byte plus the EOL \r\n
    if (serial_count > 2)
    {
        // Serial.println(serial_count);
        if (data[serial_count - 2] == '\r' && data[serial_count - 1] == '\n')
        {
            InterpretData();
            // Serial.println("Received");
        }
        ClearData();
    }
    else
    {
        ClearData();
    }
}

void InterpretData()
{
    if (data[0] == '\r' || data[0] == '\n')
    {
        return;
    }

    // if (data[0] == 'H' && data[1] == 'E')
    // {
    // }

    else if (data[0] == INCOMING_REQUEST)
    {
        switch (data[1])
        {
        case INCOMING_HANDSHAKE_REQUEST:
            Serial.write(RESPONSE_HEADER);
            Serial.write(INCOMING_HANDSHAKE_REQUEST);
            // Serial.println("Hello yourself!");
            Serial.write('\r');
            Serial.write('\n');
            break;
        case INCOMING_REQUEST_INPUT_VALUES:
            Serial.write(RESPONSE_HEADER);
            Serial.write(INCOMING_RESPONSE_INPUT_VALUES);
            Serial.write(GetAssignedInputCount());
            ReportInputValues();
            Serial.write('\r');
            Serial.write('\n');
            break;
        case INCOMING_REQUEST_DEVICE_CONFIG:
            Serial.write(RESPONSE_HEADER);
            Serial.write(INCOMING_REQUEST_DEVICE_CONFIG_RESPONSE);
            Serial.write((uint8_t)DEVICE_TYPE);
            Serial.write((uint8_t)FIRMWARE_VERSION);
            Serial.write(GetAssignedInputCount());
            Serial.write(GetMacroCount());
            Serial.write(zero); // spare
            Serial.write(zero); // spare
            Serial.write(zero); // spare
            Serial.write('\r');
            Serial.write('\n');
            break;
        case INCOMING_REQUEST_INPUT_CONFIG:
            Serial.write(RESPONSE_HEADER);
            // Byte #2 contains requested input index
            // Serial.print(data[2]);
            Serial.write(INCOMING_REQUEST_INPUT_CONFIG_RESPONSE);
            Serial.write(data[2]);
            ReportInputConfig(data[2]);
            Serial.write('\r');
            Serial.write('\n');
            break;
        case INCOMING_REQUEST_MACRO_CONFIG:
            Serial.write(RESPONSE_HEADER);
            Serial.write(INCOMING_REQUEST_MACRO_CONFIG_RESPONSE);
            Serial.write(data[2]);
            ReportMacroConfig(data[2]);
            Serial.write('\r');
            Serial.write('\n');
            break;
        case INCOMING_REQUEST_INPUT_ALL_CONFIG:
            Serial.write(RESPONSE_HEADER);
            // Byte #2 contains requested input index
            // Serial.print(data[2]);
            Serial.write(INCOMING_REQUEST_INPUT_ALL_CONFIG_RESPONSE);
            // Serial.write(0);
            Serial.write(GetAssignedInputCount());
            for (int i = 0; i < GetAssignedInputCount(); i++)
            {
                ReportInputConfig(i);
            }
            Serial.write('\r');
            Serial.write('\n');
            break;
        case INCOMING_RECEIVE_INPUT_CONFIG_UPDATE:
            Serial.write(RESPONSE_HEADER);
            // data[2] == input index

            UpdateInputConfig(data);
            Serial.write(INCOMING_RECEIVE_INPUT_CONFIG_UPDATE_RESPONSE);

            Serial.write(data[2]);
            Serial.write('o');
            Serial.write('k');

            Serial.write('\r');
            Serial.write('\n');

            break;
        case INCOMING_DELETE_INPUT_UPDATE:
            Serial.write(RESPONSE_HEADER);
            DeleteInput(data[2]);
            Serial.write(INCOMING_RESPONSE_DELETE_INPUT_UPDATE);

            Serial.write(data[2]);
            Serial.write('o');
            Serial.write('k');

            Serial.write('\r');
            Serial.write('\n');
            break;

        
        case INCOMING_REQUEST_DELETE_MACRO_UPDATE:
            Serial.write(RESPONSE_HEADER);
            DeleteMacro(data[2]);
            Serial.write(INCOMING_REQUEST_DELETE_MACRO_UPDATE_RESPONSE);

            Serial.write(data[2]);
            Serial.write('o');
            Serial.write('k');

            Serial.write('\r');
            Serial.write('\n');
            break;

        case INCOMING_REQUEST_DELETE_MACRO_BINDING_UPDATE:
            Serial.write(RESPONSE_HEADER);
            DeleteMacroBinding(data[2], data[3]);
            Serial.write(INCOMING_REQUEST_DELETE_MACRO_BINDING_UPDATE_RESPONSE);

            Serial.write(data[2]);
            Serial.write(data[3]);
            Serial.write('o');
            Serial.write('k');

            Serial.write('\r');
            Serial.write('\n');
            break;

        case INCOMING_REQUEST_MACRO_CONFIG_UPDATE:
            Serial.write(RESPONSE_HEADER);
            Serial.write(INCOMING_REQUEST_MACRO_CONFIG_UPDATE_RESPONSE);
            Serial.write(data[2]);
            AddMacro(data);
            Serial.write('o');
            Serial.write('k');
            Serial.write('\r');
            Serial.write('\n');

            break;

        case INCOMING_REQUEST_MACRO_NAME_UPDATE:
            Serial.write(RESPONSE_HEADER);
            Serial.write(INCOMING_REQUEST_MACRO_NAME_UPDATE_RESPONSE);
            SetMacroName(data);
            Serial.write('o');
            Serial.write('k');
            Serial.write('\r');
            Serial.write('\n');
            break;
        case INCOMING_REQUEST_MACRO_BINDING_UPDATE:

            UpdateMacroBindingConfig(data);

            Serial.write(RESPONSE_HEADER);
            Serial.write(INCOMING_REQUEST_MACRO_BINDING_UPDATE_RESPONSE);
            Serial.write(data[2]);
            Serial.write('o');
            Serial.write('k');
            Serial.write('\r');
            Serial.write('\n');
            break;
        case INCOMING_REQUEST_SAVE_TO_FLASH:
            Serial.write(RESPONSE_HEADER);
            Serial.write(INCOMING_RESPONSE_SAVE_TO_FLASH);
            SaveAllToNVM();
            Serial.write('o');
            Serial.write('k');

            Serial.write('\r');
            Serial.write('\n');

            break;
        default:
            Serial.write(RESPONSE_HEADER);
            Serial.println("UNKNOWN MESSAGE");
        }
    }
}

void ClearData()
{
    for (int i = 0; i < COMMAND_SIZE; i++)
    {
        data[i] = 0;
    }
    serial_count = 0;
}

#line 1 "e:\\Documents\\Arduino\\rr_controller_v2\\rr_controller_v2\\Configuration.ino"
uint8_t HIDMode = 0;

char deviceName[] = "NEW_DEVICE      "; //16 chars


uint8_t GetHIDMode(){
  return HIDMode;
}


#line 1 "e:\\Documents\\Arduino\\rr_controller_v2\\rr_controller_v2\\Inputs.ino"
// #define NONE     0
// #define GAMEPAD  1
// #define KEYBOARD 2
// #define MOUSE    3

#define INPUT_COUNT 64
#define MACRO_COUNT 16
Input inputs[INPUT_COUNT];
ButtonMacro macros[MACRO_COUNT];
HIDInterface gamepad;
uint8_t i2c_address = 0;

#define memoryLocationFirstRun 4095//1281
#define memoryLocationDeviceID 1282 // 16 chars
#define memoryLocationI2CAddress 1299
#define memoryLocationInputCount 1300
#define memoryLocationMacroCount 1301
#define memoryLocationMacros 1302
#define memoryLocationInputs 0
#define bytesPerInput 20

void InitInputs()
{

    uint8_t firstRun = Read8BitValue(memoryLocationFirstRun);
    Serial.println(firstRun);

    // Loads PWM_MAX values to inputs as defined for each microcontroller
    for (int i = 0; i < INPUT_COUNT; i++)
    {
        inputs[i].SetPWM_MAX(PWM_MAX);
    }

    if (firstRun == 255)
    {
        OnFirstRun();
    }
    else
    {
        Serial.println("LOADING CONFIG FROM NVM");
        LoadConfigFromNVM();

        Serial.println("LOADING INPUTS FROM NVM");
        LoadInputsFromNVM();

        Serial.println("LOADING MACROS FROM NVM");
        LoadMacrosFromNVM();
        delay(500);
        Serial.println(GetMacroCount());
        Serial.println("HELLO");
    }

    // for (int i = 0; i < 4; i++){
    //     Serial.print(i);
    //     Serial.print("\t");
    //     inputs[i].GetBinding().PrintMe();
    // }

    // macros[0].AddBinding(Binding(GAMEPAD, GAMEPAD_BUTTON, 1, 1));
    // macros[0].AddBinding(Binding(GAMEPAD, GAMEPAD_BUTTON, 1, 0));
    //  macros[0].AddBinding(Binding(GAMEPAD, GAMEPAD_BUTTON, 2, 1));
    //  macros[0].AddBinding(Binding(GAMEPAD, GAMEPAD_BUTTON, 2, 0));
    //  macros[0].AddBinding(Binding(GAMEPAD, GAMEPAD_BUTTON, 3, 1));
    //  macros[0].AddBinding(Binding(GAMEPAD, GAMEPAD_BUTTON, 3, 0));
    //  macros[0].AddBinding(Binding(GAMEPAD, GAMEPAD_BUTTON, 4, 1));
    //  macros[0].AddBinding(Binding(GAMEPAD, GAMEPAD_BUTTON, 4, 0));
    //  macros[0].AddBinding(Binding(GAMEPAD, GAMEPAD_BUTTON, 5, 1));
    //  macros[0].AddBinding(Binding(GAMEPAD, GAMEPAD_BUTTON, 5, 0));

    // macros[1].AddBinding(Binding(GAMEPAD, GAMEPAD_HAT, 1, 1));
    // macros[1].AddBinding(Binding(GAMEPAD, GAMEPAD_HAT, 1, 0));
    // macros[1].AddBinding(Binding(GAMEPAD, GAMEPAD_HAT, 2, 1));
    // macros[1].AddBinding(Binding(GAMEPAD, GAMEPAD_HAT, 2, 0));
    // macros[1].AddBinding(Binding(GAMEPAD, GAMEPAD_HAT, 3, 1));
    // macros[1].AddBinding(Binding(GAMEPAD, GAMEPAD_HAT, 3, 0));
    // macros[1].AddBinding(Binding(GAMEPAD, GAMEPAD_HAT, 4, 1));
    // macros[1].AddBinding(Binding(GAMEPAD, GAMEPAD_HAT, 4, 0));

    if (false)
    {

        inputs[0].SetBinding(Binding(GAMEPAD, GAMEPAD_HAT, 1, 0, CONSTANT));
        inputs[0].SetPinMode(INPUT_PULLUP);
        inputs[0].SetPin(27);
        inputs[0].SetIsAnalog(true);
        inputs[0].SetIsInverted(false);
        inputs[0].SetFilteringSamples(8);
        inputs[0].SetDeadZone(726);
        inputs[0].InitInput();

        inputs[1].SetBinding(Binding(GAMEPAD, GAMEPAD_HAT, 2, 0, CONSTANT));
        inputs[1].SetPinMode(INPUT);
        inputs[1].SetPin(35);
        inputs[1].SetIsAnalog(true);
        inputs[1].SetIsInverted(true);
        inputs[1].SetFilteringSamples(3);
        inputs[1].InitInput();

        inputs[2].SetBinding(Binding(GAMEPAD, GAMEPAD_HAT, 3, 0, CONSTANT));
        inputs[2].SetPinMode(INPUT);
        inputs[2].SetPin(39);
        inputs[2].SetIsAnalog(true);
        inputs[2].SetIsInverted(true);
        inputs[2].SetFilteringSamples(16);
        inputs[2].InitInput();

        // int testPins[] = {32, 33, 25, 26, 27, 14, 12, 13, 15, 2, 4, 16};

        // for (int i = 3; i < 3 + 12; i++)
        // {

        //     inputs[i].SetBinding(Binding(GAMEPAD, GAMEPAD_BUTTON, i - 2));
        //     inputs[i].SetPinMode(INPUT_PULLUP);
        //     inputs[i].SetPin(testPins[i - 3]);
        //     inputs[i].SetIsAnalog(false);
        //     inputs[i].SetIsInverted(true);
        //     inputs[i].InitInput();
        // }

        // int hatPins[] = {17, 18, 19, 21};

        // for (int i = 0; i < 4; i++)
        // {
        //     inputs[15 + i].SetBinding(Binding(GAMEPAD, GAMEPAD_HAT, 1 + i * 2, 0, CONSTANT));
        //     inputs[15 + i].SetPinMode(INPUT_PULLUP);
        //     inputs[15 + i].SetPin(hatPins[i]);
        //     inputs[15 + i].SetIsAnalog(false);
        //     inputs[15 + i].SetIsInverted(true);
        //     inputs[15 + i].InitInput();
        // }
    }
}

void InitHID()
{
    gamepad = HIDInterface();
    gamepad.InitHID();
}

uint8_t GetAssignedInputCount()
{
    for (int i = 0; i < INPUT_COUNT; i++)
    {
        if (inputs[i].GetPin() == 255)
        {
            return i;
        }
    }
    return 0;
}

uint8_t GetMacroCount()
{
    for (int i = 0; i < MACRO_COUNT; i++)
    {
        if (macros[i].GetBindingCount() == 0)
        {
            return i;
        }
    }
    return 0;
}

void UpdateInputs()
{
    for (int i = 0; i < INPUT_COUNT; i++)
    {
        inputs[i].UpdateState();
    }

    for (int i = 0; i < INPUT_COUNT; i++)
    {
        // Serial.print(i);
        // Serial.print("\t");
        // Serial.println(inputs[i].GetBinding().GetTrigger());
        if (inputs[i].GetBinding().GetTrigger() == 4)
        {
            // if (i == 1)
            // {
            //     Serial.println(inputs[i].GetBinding().GetDeviceType());
            // }
            SendInput(inputs[i].GetBinding());
        }
        else
        {
            switch (inputs[i].GetBinding().GetState())
            {
            case BUTTON_UP:

                break;
            case BUTTON_ON_UP:
                if (inputs[i].GetBinding().GetTrigger() == BUTTON_ON_UP)
                {
                    //Serial.println(i);
                    SendInput(inputs[i].GetBinding());
                }
                break;
            case BUTTON_DOWN:

                break;
            case BUTTON_ON_DOWN:
                if (inputs[i].GetBinding().GetTrigger() == BUTTON_ON_DOWN)
                {
                    SendInput(inputs[i].GetBinding());
                }
                break;
            }
        }
    }

    for (int i = 0; i < MACRO_COUNT; i++)
    {
        if (macros[i].IsValid())
        {
            if (macros[i].IsActive())
            {
                Binding b = macros[i].GetNextBinding();
                if (b.GetDeviceType() != NONE)
                {
                    SendInput(b);
                }
            }
        }
    }

    gamepad.SendReport();
}

void SendInput(Binding binding)
{
    // Serial.println("SendInput()");
    // Serial.println(binding.GetDeviceType());
    switch (binding.GetDeviceType())
    {
    case NONE:

        break;
    case GAMEPAD:
        // Temp fix which fits macros in under gamepad
        //(should be own device type but device types not implemented yet)
        if (binding.GetInputType() == 4)
        {
            macros[binding.GetAssignedInput()].StartMacro();
        }
        else
        {
            SendGamepadInput(binding);
        }
        break;
    case KEYBOARD:

        break;
    case MOUSE:

        break;
    case MACRO:
        // Serial.println("MACRO");
        if (binding.GetVal() == 1)
        {
            // Serial.println("START MACRO");
            macros[binding.GetAssignedInput()].StartMacro();
        }
        break;
    }
}

void SendGamepadInput(Binding binding)
{
    // Serial.println(binding.GetInputType());
    switch (binding.GetInputType())
    {
    case GAMEPAD_BUTTON:
        gamepad.SetGamepadButton(binding.GetAssignedInput(),
                                 binding.GetVal());
        break;
    case GAMEPAD_AXIS:
        gamepad.SetGamepadAxis(binding.GetAssignedInput(),
                               binding.GetVal());
        break;
    case GAMEPAD_HAT:
        if (binding.GetVal() == 1)
        {
            gamepad.SetGamepadHat(binding.GetAssignedInput());
        }
        break;
    case GAMEPAD_SPECIAL:
        gamepad.SetGamepadSpecial(binding.GetAssignedInput(),
                                  binding.GetVal());
        break;
    }
}

void ReportInputConfig(uint8_t index)
{
    if (index >= 0 && index < INPUT_COUNT)
    {
        inputs[index].ReportConfig();
    }
}

void AddMacro(char *data)
{
    if (data[2] >= 0 && data[2] <= MACRO_COUNT)
    {
        macros[data[2]].NewMacro(data);
    }
}

void SetMacroName(char *data)
{

    if (data[2] >= 0 && data[2] <= MACRO_COUNT)
    {
        macros[data[2]].SetMacroName(data);
    }
}

void ReportMacroConfig(uint8_t index)
{
    if (index >= 0 && index < MACRO_COUNT)
    {
        macros[index].ReportConfig();
    }
}

void ReportInputValues()
{
    for (int i = 0; i < GetAssignedInputCount(); i++)
    {
        inputs[i].ReportInputValues();
    }
}

void UpdateInputConfig(char *data)
{
    inputs[data[2]].UpdateInputConfig(data);
}

void DeleteInput(uint8_t idx)
{
    for (int i = idx; i < GetAssignedInputCount(); i++)
    {
        inputs[i] = inputs[i + 1];
    }
    inputs[GetAssignedInputCount()].SetToDefaults();
}

void DeleteMacro(uint8_t idx)
{
    for (int i = idx; i < GetMacroCount(); i++)
    {
        macros[i] = macros[i + 1];
    }
    macros[GetMacroCount()].SetToDefaults();
}

void DeleteMacroBinding(uint8_t macroIdx, uint8_t bindingIdx)
{
    macros[macroIdx].DeleteBinding(bindingIdx);
}

void UpdateMacroBindingConfig(char *data)
{
    macros[data[2]].SetBindingConfig(data);
}

void LoadConfigFromNVM()
{
    // Load deviceName from EEPROM
    for (int i = 0; i < 16; i++)
    {
        deviceName[i] = char(Read8BitValue(memoryLocationDeviceID + i));
    }

    // Load i2cAddress from EEPROM
    i2c_address = Read8BitValue(memoryLocationI2CAddress);
}

void LoadInputsFromNVM()
{
    uint8_t inputCount = Read8BitValue(memoryLocationInputCount);
    char data[32];
    for (int i = 0; i < 32; i++)
    {
        data[i] = 0;
    }
    for (int i = 0; i < inputCount; i++)
    {
        for (int b = 0; b < 23; b++)
        {
            data[b] = Read8BitValue(memoryLocationInputs + i * bytesPerInput + b);
            // Serial.print(b);
            // Serial.print(' ');
            // Serial.println(int(data[b]));
        }
        ////  Serial.println();

        inputs[i].UpdateInputConfig(data);
    }
}

void LoadMacrosFromNVM()
{
    uint8_t macroCount = Read8BitValue(memoryLocationMacroCount);
    int currentByte = memoryLocationMacros;
    for (int i = 0; i < macroCount; i++)
    {
        for (int n = 0; n < 16; n++)
        {
            macros[i].macroName[n] = Read8BitValue(currentByte + n);
        }
        currentByte += 16;
        // macros[i].AddBinding(Binding());

        uint8_t bindingCount = Read8BitValue(currentByte);
        Serial.print("Binding count: ");
        Serial.println(bindingCount);
        currentByte += 1;
        char data[11];
        for (uint8_t z = 0; z < bindingCount; z++)
        {
            data[2] = i;
            data[3] = z;
            for (uint8_t b = 4; b < 11; b++)
            {
                data[b] = Read8BitValue(currentByte);
                currentByte += 1;
            }
            macros[i].AddBinding(Binding());
            macros[i].SetBindingConfig(data);
        }
    }
}

void SaveAllToNVM()
{
    // Save deviceName to NVM
    for (int i = 0; i < 16; i++)
    {
        Store8BitValue(memoryLocationDeviceID + i, deviceName[i]);
    }

    // Save i2cAddress to NVM
    Store8BitValue(memoryLocationI2CAddress, i2c_address);

    Store8BitValue(memoryLocationInputCount, GetAssignedInputCount());
    Serial.write(GetAssignedInputCount());
    for (int i = 0; i < GetAssignedInputCount() + 1; i++)
    {
        Store8BitValue(memoryLocationInputs + i * bytesPerInput + 3, inputs[i].GetPin());
        Store8BitValue(memoryLocationInputs + i * bytesPerInput + 4, inputs[i].GetPinMode());
        Store8BitValue(memoryLocationInputs + i * bytesPerInput + 5, inputs[i].IsAnalog());
        Store8BitValue(memoryLocationInputs + i * bytesPerInput + 6, inputs[i].GetIsInverted());

        Store8BitValue(memoryLocationInputs + i * bytesPerInput + 7, highByte(inputs[i].GetMinVal()));
        Store8BitValue(memoryLocationInputs + i * bytesPerInput + 8, lowByte(inputs[i].GetMinVal()));
        Store8BitValue(memoryLocationInputs + i * bytesPerInput + 9, highByte(inputs[i].GetMidVal()));
        Store8BitValue(memoryLocationInputs + i * bytesPerInput + 10, lowByte(inputs[i].GetMidVal()));
        Store8BitValue(memoryLocationInputs + i * bytesPerInput + 11, highByte(inputs[i].GetMaxVal()));
        Store8BitValue(memoryLocationInputs + i * bytesPerInput + 12, lowByte(inputs[i].GetMaxVal()));

        Store8BitValue(memoryLocationInputs + i * bytesPerInput + 13, highByte(inputs[i].GetDeadZone()));
        Store8BitValue(memoryLocationInputs + i * bytesPerInput + 14, lowByte(inputs[i].GetDeadZone()));
        Store8BitValue(memoryLocationInputs + i * bytesPerInput + 15, inputs[i].GetBufferSize());

        Store8BitValue(memoryLocationInputs + i * bytesPerInput + 16, inputs[i].GetBinding().GetDeviceType());
        Store8BitValue(memoryLocationInputs + i * bytesPerInput + 17, inputs[i].GetBinding().GetInputType());
        Store8BitValue(memoryLocationInputs + i * bytesPerInput + 18, inputs[i].GetBinding().GetAssignedInput());

        Store8BitValue(memoryLocationInputs + i * bytesPerInput + 19, highByte(inputs[i].GetBinding().GetVal()));

        Store8BitValue(memoryLocationInputs + i * bytesPerInput + 20, lowByte(inputs[i].GetBinding().GetVal()));

        Store8BitValue(memoryLocationInputs + i * bytesPerInput + 22, inputs[i].GetBinding().GetTrigger());
    }

    Store8BitValue(memoryLocationMacroCount, GetMacroCount());
    Serial.write(123);
    Serial.write(GetMacroCount());
    int currentByte = memoryLocationMacros;
    for (int i = 0; i < GetMacroCount(); i++)
    {
        for (int n = 0; n < 16; n++)
        {
            Store8BitValue(currentByte + n, macros[i].macroName[n]);
        }
        currentByte += 16;

        uint8_t bindingCount = macros[i].GetBindingCount();
        Store8BitValue(currentByte, bindingCount);
        currentByte++;

        for (int b = 0; b < bindingCount; b++)
        {
            Store8BitValue(currentByte + 0, macros[i].GetBinding(b).GetDeviceType());
            Store8BitValue(currentByte + 1, macros[i].GetBinding(b).GetInputType());
            Store8BitValue(currentByte + 2, macros[i].GetBinding(b).GetAssignedInput());
            Store8BitValue(currentByte + 3, highByte(macros[i].GetBinding(b).GetVal()));
            Store8BitValue(currentByte + 4, lowByte(macros[i].GetBinding(b).GetVal()));
            Store8BitValue(currentByte + 5, macros[i].GetBinding(b).GetState());
            Store8BitValue(currentByte + 6, macros[i].GetBinding(b).GetTrigger());
            currentByte += 7;
        }
    }
}

void OnFirstRun()
{
    Serial.println("First Run");
    char defaultName[] = "NEW_DEVICE      ";
    for (int i = 0; i < 16; i++)
    {
        Store8BitValue(memoryLocationDeviceID + i, defaultName[i]);
    }

    // Write default i2c_address to EEPROM
    Store8BitValue(memoryLocationI2CAddress, 0);

    Store8BitValue(memoryLocationFirstRun, 1); // mark first run byte

    Store8BitValue(memoryLocationInputCount, 0);

    Store8BitValue(memoryLocationMacroCount, 0);
}

#line 1 "e:\\Documents\\Arduino\\rr_controller_v2\\rr_controller_v2\\NonVolatileStorage.ino"
// Non-volatile memory interface
// Accesses EEPROM for Atmega32U4 devices and preferences for ESP32
// Limited by Atmega32U4 1024 bytes of EEPROM memory

// 0 - 20      inputs[0]
// 0 - 1280 (20*64)    inputs[*]
// 800 - 847   ENCODER_ASSIGNEMENTS (48)
// 1000 - 1015 DEVICE_NAME
// 1022        I2C_ADDRESS
// 1024        FIRSTRUN (100 == false)

// ATMEGA 2560
// 0 - 14      inputs[0]
// 0 - 882     inputs[*]
// 2012 - 2043   MATRIX_PIN_ASSIGNMENTS (32)
// 2044 - 2299   MATRIX_BUTTON_ASSIGNMENTS (256)
// 2300 - 2347   ENCODER_ASSIGNEMENTS (48)
// 4000 - 4015 DEVICE_NAME
// 4022        I2C_ADDRESS
// 4024        FIRSTRUN (100 == false)
#include <EEPROM.h>
#define EEPROM_SIZE 4096

void InitNVM()
{
    EEPROM.begin(EEPROM_SIZE);
    
}

void Store8BitValue(uint16_t idx, uint8_t val)
{
    EEPROM.write(idx, val);
    EEPROM.commit();
}

uint8_t Read8BitValue(uint16_t idx)
{
    return EEPROM.read(idx);
}

