#line 1 "e:\\Documents\\Arduino\\rr_controller_v2\\rr_controller_v2\\BLEHIDInterface.h"
#include <BleGamepad.h>

#define numOfButtons 128
#define numOfHatSwitches 4

#define enableX true
#define enableY true
#define enableZ true
#define enableRX true
#define enableRY true
#define enableRZ true
#define enableSlider1 true
#define enableSlider2 true

#define enableSteering false    // X
#define enableAccelerator false // Y
#define enableRudder false     // RZ / RUDDER
#define enableBrake false       // RZ / RUDDER
#define enableThrottle false    // SLIDER 2

class HIDInterface
{

private:
    BleGamepad bleGamepad;
    BleGamepadConfiguration bleGamepadConfig;

    std::string btName = "RealRobots HID Device";
    std::string btManufacturer = "RealRobots";

    uint8_t hats[4] = {};

public:
    void InitHID()
    {
        Serial.println("Starting BLE work!");
        // bleGamepad = BleGamepad(btName, btManufacturer, 100);
        bleGamepadConfig.setAutoReport(false);
        bleGamepadConfig.setControllerType(CONTROLLER_TYPE_GAMEPAD); // CONTROLLER_TYPE_JOYSTICK, CONTROLLER_TYPE_GAMEPAD (DEFAULT), CONTROLLER_TYPE_MULTI_AXIS
        bleGamepadConfig.setButtonCount(numOfButtons);
        bleGamepadConfig.setHatSwitchCount(numOfHatSwitches);
        // bleGamepadConfig.setWhichSpecialButtons(true, true, true, true, true, true, true, true);
        bleGamepadConfig.setVid(0xe502);
        bleGamepadConfig.setPid(0xabcd);
        bleGamepadConfig.setAxesMin(0x0000); // 0 --> int16_t - 16 bit signed integer - Can be in decimal or hexadecimal
        bleGamepadConfig.setAxesMax(0x7FFF); // 32767 --> int16_t - 16 bit signed integer - Can be in decimal or hexadecimal
        bleGamepadConfig.setWhichAxes(enableX, enableY, enableZ, enableRX, enableRY, enableRZ, enableSlider1, enableSlider2);
        bleGamepadConfig.setWhichSimulationControls(enableRudder, enableThrottle, enableAccelerator, enableBrake, enableSteering);

        bleGamepad.begin(&bleGamepadConfig);
    }

    void SetGamepadButton(uint8_t button, uint8_t state)
    {
        if (state)
        {
            bleGamepad.press(button);
        }
        else
        {
            bleGamepad.release(button);
        }
    }

    // Special buttons not working, not sure why
    void SetGamepadSpecial(uint8_t button, uint8_t state)
    {
        if (state)
        {
            bleGamepad.pressSpecialButton(button);
        }
        else
        {
            bleGamepad.pressSpecialButton(button);
        }
    }

    // Hat number = hatIdx/9, hat position = hatIdx % 9
    void SetGamepadHat(uint8_t hatIdx)
    {
        hats[hatIdx / 9] = hatIdx % 9;
    }

    void SetGamepadAxis(uint8_t axis, int16_t value)
    {
        // Serial.print(axis);
        // Serial.print("\t");
        // Serial.println(value);
        switch (axis)
        {
        case 0: // X Axis
            bleGamepad.setX(value);
            break;
        case 1: // Y Axis
            bleGamepad.setY(value);
            break;
        case 2: // Z Axis
            bleGamepad.setZ(value);
            break;
        case 3: // rX Axis
            bleGamepad.setRX(value);
            break;
        case 4: // rY Axis
            bleGamepad.setRY(value);
            break;
        case 5: // rZ Axis
            bleGamepad.setRZ(value);
            break;
        case 6: // Slider1 Axis
            bleGamepad.setSlider1(value);
            break;
        case 7: // Slider2 Axis
            bleGamepad.setSlider2(value);
            break;
        case 8:
            bleGamepad.setSteering(value);
            break;
        case 9:
            bleGamepad.setAccelerator(value);
            break;
        case 10:
            bleGamepad.setRudder(value);
            break;
        case 11:
            bleGamepad.setBrake(value);
            break;
        case 12:
            bleGamepad.setThrottle(value);
            break;
        case 13:
            bleGamepad.setBatteryLevel(map(value, 0, 32767, 0, 100));
            break;
        }
    }

    void SendHats()
    {
        bleGamepad.setHats(hats[0], hats[1], hats[2], hats[3]);
        for (int i = 0; i < 4; i++)
        {
            hats[i] = 0;
        }
    }

    void SendReport()
    {
        SendHats();

        if (bleGamepad.isConnected()){
            bleGamepad.sendReport();
        }
    }
};