
// #define PWM_MAX 4096

#define ROLLING_AVG_MAX 32

// Input Type
#define NONE 0
#define GAMEPAD 1
#define KEYBOARD 2
#define MOUSE 3
#define MACRO 4

// special MACRO input entries
#define DELAY 1
#define REPEAT 2

// GAMEPAD input types
#define GAMEPAD_BUTTON 1  // 1-128
#define GAMEPAD_AXIS 2    // 0-7  x, y, z, rZ, rX, rY, slider1, slider2
#define GAMEPAD_HAT 3     // 3 hats, 8 directions each, assignedInput = dir + hatIdx*9
#define GAMEPAD_SPECIAL 4 // 0=Start, 1=Select, 2=Menu, 3=Home, 4=Back, 5=Volume_inc, 6=Volume_dec, 7=Volume_mute

#define XAXIS 0
#define YAXIS 1
#define ZAXIS 2
#define RXAXIS 3
#define RYAXIS 4
#define RZAXIS 5
#define SLIDER1 6
#define SLIDER2 7

#define STEERING 8
#define ACCELERATOR 9
#define RUDDER 10
#define BRAKE 11
#define THROTTLE 12

#define BATTERY_LEVEL 13

#define BUTTON_UP 0
#define BUTTON_DOWN 1
#define BUTTON_ON_UP 2
#define BUTTON_ON_DOWN 3
#define CONSTANT 4

class Binding
{
private:
    // Type of device, GAMPAD, KEYBOARD or MOUSE
    uint8_t _deviceType = NONE;

    // Type of input under the deviceType, ie. GAMEPAD_BUTTON, MOUSE_AXES etc
    uint8_t _inputType = NONE;

    // Actual assigned input eg, button 23 or axes 6 (slider1)
    uint8_t _assignedInput = 0;

    int16_t _val;
    int16_t _rawVal;
    int16_t _prev;
    uint8_t _state = BUTTON_UP;
    uint8_t _trigger = BUTTON_ON_DOWN;

public:
    Binding()
    {
        _deviceType = 1;
        _inputType = 1;
        _assignedInput = 0;
    }

    Binding(uint8_t deviceType, uint8_t inputType, uint8_t assignedInput)
    {
        _deviceType = deviceType;
        _inputType = inputType;
        _assignedInput = assignedInput;
    }

    Binding(uint8_t deviceType, uint8_t inputType, uint8_t assignedInput, int16_t val)
    {
        _deviceType = deviceType;
        _inputType = inputType;
        _assignedInput = assignedInput;
        _val = val;
    }

    Binding(uint8_t deviceType, uint8_t inputType, uint8_t assignedInput, int16_t val, uint8_t trigger)
    {
        _deviceType = deviceType;
        _inputType = inputType;
        _assignedInput = assignedInput;
        _val = val;
        _trigger = trigger;

        // TODO: Change this when other devices are added to configurator
        // Right now MACROS are added as a fourth type of GAMEPAD input
        if (_inputType == 4){
            _deviceType == 4;
        }
    }

    void ReportBinding() // 7 bytes
    {
        Serial.write(_deviceType);
        Serial.write(_inputType);
        Serial.write(_assignedInput);

        Serial.write(highByte(_val));
        Serial.write(lowByte(_val));

        Serial.write(_state);
        Serial.write(_trigger);
    }

    uint8_t GetDeviceType()
    {
        return _deviceType;
    }

    void SetDeviceType(uint8_t deviceType)
    {
        _deviceType = deviceType;
    }

    uint8_t GetInputType()
    {
        return _inputType;
    }

    void SetInputType(uint8_t inputType)
    {
        _inputType = inputType;
    }

    uint8_t GetAssignedInput()
    {
        return _assignedInput;
    }

    void SetAssignedInput(uint8_t assignedInput)
    {
        _assignedInput = assignedInput;
    }

    void SetHatBinding(uint8_t hatIdx, uint8_t hatDir)
    {
        _assignedInput = hatDir + (9 * hatIdx);
    }

    void SetVal(int16_t val)
    {
        if (_val == 1 && _prev == 1)
        {
            _state = BUTTON_DOWN;
        }
        else if (_val == 1 && _prev == 0)
        {
            _state = BUTTON_ON_DOWN;
        }
        else if (_val == 0 && _prev == 0)
        {
            _state = BUTTON_UP;
        }
        else if (_val == 0 && _prev == 1)
        {
            _state = BUTTON_ON_UP;
        }

        _prev = _val;
        _val = val;
    }

    int16_t GetVal()
    {
        return _val;
    }

    void SetRawVal(int16_t val)
    {
        _rawVal = val;
    }

    int16_t GetRawVal()
    {
        return _rawVal;
    }

    void SetState(uint8_t state)
    {
        _state = state;
    }

    uint8_t GetState()
    {
        return _state;
    }

    void SetTrigger(uint8_t trigger)
    {
        _trigger = trigger;
    }

    uint8_t GetTrigger()
    {
        return _trigger;
    }

    void PrintMe()
    {
        Serial.print(_deviceType);
        Serial.print("\t");
        Serial.print(_inputType);
        Serial.print("\t");
        Serial.print(_assignedInput);
        Serial.print("\t");
        Serial.print(_state);
        Serial.print("\t");
        Serial.print(_trigger);
        Serial.print("\t");
        Serial.print(_val);
        Serial.println();
    }

    // Didn't work, have to set in constructor
    // void SetTrigger(uint8_t trigger){
    //     _trigger = trigger;
    //     Serial.print("Trigger set to ");
    //     Serial.println(_trigger);
    // }
};

class ButtonMacro
{
private:
    Binding bindingList[32];
    uint8_t bindingCount = 0;
    uint8_t currentBinding = 0;
    long timeOfLastPress = 0;
    long pressDelay = 100;
    bool isActive = false;
    bool cleaningUp = true;

public:
    char macroName[16]; // 16 chars

    ButtonMacro()
    {
        macroName[0] = 'M';
        macroName[1] = 'a';
        macroName[2] = 'c';
        macroName[3] = 'r';
        macroName[4] = 'o';
    }

    void NewMacro(char *data)
    {
        for (int i = 0; i < 16; i++)
        {
            macroName[i] = data[i + 3];
        }
        bindingCount = 0;
        AddBinding(Binding());
    }

    void SetToDefaults()
    {
        for (int i = 0; i < 16; i++)
        {
            macroName[i] = 32; // space
        }
        bindingCount = 0;
    }

    void SetMacroName(char *data)
    {
        for (int i = 0; i < 16; i++)
        {
            macroName[i] = data[i + 3];
        }
    }

    bool IsValid()
    {
        return bindingCount > 0;
    }

    bool IsActive()
    {
        return isActive;
    }

    void StartMacro()
    {
        if (currentBinding != 0)
        {
            cleaningUp = true;
            currentBinding = 0;
        }
        isActive = true;
    }

    void AddBinding(Binding binding)
    {
        bindingList[bindingCount] = binding;
        bindingCount++;
    }

    Binding GetNextBinding()
    {
        if (cleaningUp)
        {

            uint8_t targetBinding = currentBinding;
            currentBinding++;
            if (currentBinding > bindingCount)
            {
                currentBinding = 0;
                cleaningUp = false;
            }

            if (bindingList[currentBinding - 1].GetVal() == 0)
            {
                return bindingList[currentBinding - 1];
            }
            else
            {
                return Binding(NONE, 0, 0);
            }
        }
        else if (millis() - timeOfLastPress > pressDelay)
        {
            timeOfLastPress = millis();
            uint8_t targetBinding = currentBinding;
            currentBinding++;
            if (currentBinding > bindingCount)
            {
                ResetMacro();
            }
            return bindingList[currentBinding - 1];
        }

        return Binding(NONE, 0, 0);
    }

    uint8_t GetBindingCount()
    {
        return bindingCount;
    }

    void ResetMacro()
    {
        isActive = false;
        currentBinding = 0;
        timeOfLastPress = 0;
    }

    void ReportConfig() // 13 bytes
    {
        for (int i = 0; i < 16; i++)
        {
            Serial.write(macroName[i]);
        }
        Serial.write(bindingCount);
        for (int i = 0; i < bindingCount; i++)
        {
            bindingList[i].ReportBinding();
        }
    }

    Binding GetBinding(uint8_t idx)
    {
        return bindingList[idx];
    }

    void SetBindingConfig(char *data)
    {
        if (bindingCount < data[3]+1){
            bindingCount = data[3]+1;
        }

        bindingList[data[3]].SetDeviceType(data[4]);
        bindingList[data[3]].SetInputType(data[5]);
        bindingList[data[3]].SetAssignedInput(data[6]);

        uint8_t hb = data[7];
        uint8_t lb = data[8];
        int16_t v = (hb << 8) + lb;
        bindingList[data[3]].SetVal(v);

        bindingList[data[3]].SetState(data[9]);
        bindingList[data[3]].SetTrigger(data[10]);
    }

    void DeleteBinding(uint8_t idx)
    {
        for (int i = idx; i < bindingCount; i++)
        {
            bindingList[i] = bindingList[i + 1];
        }
        bindingCount    -= 1;
    }
};

class Input
{
private:
    // int16_t _val = 0;
    // int16_t _rawVal = 0;
    int16_t _prev;
    uint8_t _pin = 255;
    uint8_t _pinMode = INPUT_PULLUP;
    uint8_t _isAnalog = false;
    int16_t _minVal = 0;
    int16_t _midVal = 0; // PWM_MAX / 2;
    int16_t _maxVal = 0; // PWM_MAX;
    int16_t _deadZone = 8;
    uint8_t _isInverted = false;

    uint8_t _bufferSize = 1;
    int16_t buffer[ROLLING_AVG_MAX];
    uint8_t bufferIdx;
    int16_t PWM_MAX = 4095;

    Binding _binding = Binding(NONE, NONE, 0);

public:
    Input()
    {
    }

    void InitInput()
    {
        pinMode(_pin, _pinMode);
    }

    void SetPWM_MAX(int16_t val)
    {
        PWM_MAX = val;
        _midVal = PWM_MAX / 2;
        _maxVal = PWM_MAX;
    }

    void SetToDefaults()
    {
        _pin = 255;
        _pinMode = INPUT_PULLUP;
        _isAnalog = false;
        _minVal = 0;
        _midVal = PWM_MAX / 2;
        _maxVal = PWM_MAX;
        _deadZone = 128;
        _isInverted = false;
        _binding = Binding(NONE, NONE, 0);
    }

    void ReportConfig() // 13 bytes
    {
        Serial.write(_pin);
        Serial.write(_pinMode);
        Serial.write(_isAnalog);
        Serial.write(_isInverted);

        Serial.write(highByte(_minVal));
        Serial.write(lowByte(_minVal));

        Serial.write(highByte(_midVal));
        Serial.write(lowByte(_midVal));

        Serial.write(highByte(_maxVal));
        Serial.write(lowByte(_maxVal));

        Serial.write(highByte(_deadZone));
        Serial.write(lowByte(_deadZone));

        Serial.write(_bufferSize);

        _binding.ReportBinding();
    }

    void SetPin(uint8_t pin)
    {
        _pin = pin;
    }

    uint8_t GetPin()
    {
        return _pin;
    }

    void SetPinMode(uint8_t pinMode)
    {
        _pinMode = pinMode;
    }

    uint8_t GetPinMode()
    {
        return _pinMode;
    }

    void SetBinding(Binding binding)
    {
        _binding = binding;
    }

    Binding GetBinding()
    {
        return _binding;
    }

    void UpdateState()
    {

        if (_binding.GetDeviceType() == NONE)
        {
            return;
        }

        int16_t _val;

        if (_isAnalog)
        {
            _val = analogRead(_pin);
            _binding.SetRawVal(_val);
            if (_isInverted)
            {
                _val = PWM_MAX - _val;
            }
            long temp; // = map(_val, _minVal, _maxVal, 0, 32767);

            if (_val < _midVal - _deadZone)
            {
                temp = map(_val, _minVal, _midVal - _deadZone, 0, 32767 / 2);
            }
            else if (_val > _midVal + _deadZone)
            {
                temp = map(_val, _midVal + _deadZone, _maxVal, 32767 / 2, 32767);
            }
            else
            {
                temp = 32767 / 2;
            }

            if (temp < 0)
            {
                _val = 0;
            }
            else if (temp > 32767)
            {
                _val = 32767;
            }
            else
            {
                _val = temp;
            }
            //_val = map(_val, _minVal, _maxVal, 0, 32767);
        }
        else
        {
            _val = digitalRead(_pin);
            _binding.SetRawVal(_val);
            if (_isInverted)
            {
                _val = !_val;
            }
        }

        if (_bufferSize > 1)
        {
            // ROLLING AVERAGE FILTER
            buffer[bufferIdx] = _val;
            bufferIdx++;
            if (bufferIdx == _bufferSize)
            {
                bufferIdx = 0;
            }
            // Serial.print(_val);
            // Serial.print("\t");

            int32_t total = 0;
            for (int i = 0; i < _bufferSize; i++)
            {
                total += buffer[i];
            }

            _val = total / (int32_t)_bufferSize;
            // Serial.println(_val);
        }

        _binding.SetVal(_val);
    }

    void ReportInputValues()
    {
        Serial.write(highByte(_binding.GetRawVal()));
        Serial.write(lowByte(_binding.GetRawVal()));
        Serial.write(highByte(_binding.GetVal()));
        Serial.write(lowByte(_binding.GetVal()));
    }

    void SetIsAnalog(uint8_t isAnalog)
    {
        _isAnalog = isAnalog;
    }

    bool IsAnalog()
    {
        return _isAnalog;
    }

    // int16_t GetVal()
    // {
    //     return _val;
    // }

    void SetIsInverted(uint8_t isInverted)
    {
        _isInverted = isInverted;
    }

    uint8_t GetIsInverted()
    {
        return _isInverted;
    }

    void SetMinVal(int16_t minVal)
    {
        _minVal = minVal;
    }

    int16_t GetMinVal()
    {
        return _minVal;
    }

    void SetMidVal(int16_t midVal)
    {
        _midVal = midVal;
    }

    int16_t GetMidVal()
    {
        return _midVal;
    }

    void SetMaxVal(int16_t maxVal)
    {
        _maxVal = maxVal;
    }

    int16_t GetMaxVal()
    {
        return _maxVal;
    }

    void SetDeadZone(int16_t deadZone)
    {
        _deadZone = deadZone;
    }

    int16_t GetDeadZone()
    {
        return _deadZone;
    }

    void SetFilteringSamples(uint8_t samples)
    {
        _bufferSize = samples;
    }

    uint8_t GetBufferSize()
    {
        return _bufferSize;
    }

    void UpdateInputConfig(char *data)
    {

        SetPin(data[3]);
        SetPinMode(data[4]);
        SetIsAnalog(data[5]);
        SetIsInverted(data[6]);

        uint8_t hb = data[7];
        uint8_t lb = data[8];
        int16_t v = (hb << 8) + lb;
        _minVal = v;

        hb = data[9];
        lb = data[10];
        v = (hb << 8) + lb;
        _midVal = v;

        hb = data[11];
        lb = data[12];
        v = (hb << 8) + lb;
        _maxVal = v;

        hb = data[13];
        lb = data[14];
        v = (hb << 8) + lb;
        _deadZone = v;

        _bufferSize = data[15];

        hb = data[19];
        lb = data[20];
        v = (hb << 8) + lb;

        _binding = Binding(data[16], data[17], data[18], v, data[22]);
        InitInput();
    }
};