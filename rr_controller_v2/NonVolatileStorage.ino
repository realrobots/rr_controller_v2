// Non-volatile memory interface
// Accesses EEPROM for Atmega32U4 devices and preferences for ESP32
// Limited by Atmega32U4 1024 bytes of EEPROM memory

// 0 - 20      inputs[0]
// 0 - 1280 (20*64)    inputs[*]
// 800 - 847   ENCODER_ASSIGNEMENTS (48)
// 1000 - 1015 DEVICE_NAME
// 1022        I2C_ADDRESS
// 1024        FIRSTRUN (100 == false)

// ATMEGA 2560
// 0 - 14      inputs[0]
// 0 - 882     inputs[*]
// 2012 - 2043   MATRIX_PIN_ASSIGNMENTS (32)
// 2044 - 2299   MATRIX_BUTTON_ASSIGNMENTS (256)
// 2300 - 2347   ENCODER_ASSIGNEMENTS (48)
// 4000 - 4015 DEVICE_NAME
// 4022        I2C_ADDRESS
// 4024        FIRSTRUN (100 == false)
#include <EEPROM.h>
#define EEPROM_SIZE 4096

void InitNVM()
{
    EEPROM.begin(EEPROM_SIZE);
    
}

void Store8BitValue(uint16_t idx, uint8_t val)
{
    EEPROM.write(idx, val);
    EEPROM.commit();
}

uint8_t Read8BitValue(uint16_t idx)
{
    return EEPROM.read(idx);
}
