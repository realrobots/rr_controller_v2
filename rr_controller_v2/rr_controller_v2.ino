#include "InputClass.h"


// To switch between microcontroller types
// define only that one below
// and assign to to DEVICE_TYPE

// #define DUMMY
// #define ESP32 0
// #define ESP32S2 1
// #define ESP32S3 2
#define RP2040 3

#define FIRMWARE_VERSION 2
#define DEVICE_TYPE RP2040

// #define USBMODE
//#define BLEMODE
//#define DUMMYMODE

#ifdef ESP32
#include "BLEHIDInterface.h"
#define PWM_MAX 4095
#endif

#ifdef RP2040
#include "RP2040USBHIDInterface.h"
#define PWM_MAX 1023
#endif

#ifdef DUMMY
#include "DummyHIDInterface.h"
#define PWM_MAX 4095
#endif

long lastLoop;
long loopInterval = 1000 / 100;

void setup()
{
  // put your setup code here, to run once:

  pinMode(2, OUTPUT);
  for (int i = 0; i < 4; i++)
  {
    digitalWrite(2, HIGH);
    delay(50);
    digitalWrite(2, LOW);
    delay(50);
  }

  InitComms();
  InitNVM();
  // pinMode(15, OUTPUT);
  // digitalWrite(15, 1);
  InitInputs();
  InitHID();

  

}

void loop()
{
  // put your main code here, to run repeatedly:
  if (millis() - lastLoop > loopInterval)
  {
    UpdateInputs();
    lastLoop = millis();
  }

  CheckComms();

}
