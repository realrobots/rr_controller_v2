// #define NONE     0
// #define GAMEPAD  1
// #define KEYBOARD 2
// #define MOUSE    3

#define INPUT_COUNT 64
#define MACRO_COUNT 16
Input inputs[INPUT_COUNT];
ButtonMacro macros[MACRO_COUNT];
HIDInterface gamepad;
uint8_t i2c_address = 0;

#define memoryLocationFirstRun 4095//1281
#define memoryLocationDeviceID 1282 // 16 chars
#define memoryLocationI2CAddress 1299
#define memoryLocationInputCount 1300
#define memoryLocationMacroCount 1301
#define memoryLocationMacros 1302
#define memoryLocationInputs 0
#define bytesPerInput 20

void InitInputs()
{

    uint8_t firstRun = Read8BitValue(memoryLocationFirstRun);
    Serial.println(firstRun);

    // Loads PWM_MAX values to inputs as defined for each microcontroller
    for (int i = 0; i < INPUT_COUNT; i++)
    {
        inputs[i].SetPWM_MAX(PWM_MAX);
    }

    if (firstRun == 255)
    {
        OnFirstRun();
    }
    else
    {
        Serial.println("LOADING CONFIG FROM NVM");
        LoadConfigFromNVM();

        Serial.println("LOADING INPUTS FROM NVM");
        LoadInputsFromNVM();

        Serial.println("LOADING MACROS FROM NVM");
        LoadMacrosFromNVM();
        delay(500);
        Serial.println(GetMacroCount());
        Serial.println("HELLO");
    }

    // for (int i = 0; i < 4; i++){
    //     Serial.print(i);
    //     Serial.print("\t");
    //     inputs[i].GetBinding().PrintMe();
    // }

    // macros[0].AddBinding(Binding(GAMEPAD, GAMEPAD_BUTTON, 1, 1));
    // macros[0].AddBinding(Binding(GAMEPAD, GAMEPAD_BUTTON, 1, 0));
    //  macros[0].AddBinding(Binding(GAMEPAD, GAMEPAD_BUTTON, 2, 1));
    //  macros[0].AddBinding(Binding(GAMEPAD, GAMEPAD_BUTTON, 2, 0));
    //  macros[0].AddBinding(Binding(GAMEPAD, GAMEPAD_BUTTON, 3, 1));
    //  macros[0].AddBinding(Binding(GAMEPAD, GAMEPAD_BUTTON, 3, 0));
    //  macros[0].AddBinding(Binding(GAMEPAD, GAMEPAD_BUTTON, 4, 1));
    //  macros[0].AddBinding(Binding(GAMEPAD, GAMEPAD_BUTTON, 4, 0));
    //  macros[0].AddBinding(Binding(GAMEPAD, GAMEPAD_BUTTON, 5, 1));
    //  macros[0].AddBinding(Binding(GAMEPAD, GAMEPAD_BUTTON, 5, 0));

    // macros[1].AddBinding(Binding(GAMEPAD, GAMEPAD_HAT, 1, 1));
    // macros[1].AddBinding(Binding(GAMEPAD, GAMEPAD_HAT, 1, 0));
    // macros[1].AddBinding(Binding(GAMEPAD, GAMEPAD_HAT, 2, 1));
    // macros[1].AddBinding(Binding(GAMEPAD, GAMEPAD_HAT, 2, 0));
    // macros[1].AddBinding(Binding(GAMEPAD, GAMEPAD_HAT, 3, 1));
    // macros[1].AddBinding(Binding(GAMEPAD, GAMEPAD_HAT, 3, 0));
    // macros[1].AddBinding(Binding(GAMEPAD, GAMEPAD_HAT, 4, 1));
    // macros[1].AddBinding(Binding(GAMEPAD, GAMEPAD_HAT, 4, 0));

    if (false)
    {

        inputs[0].SetBinding(Binding(GAMEPAD, GAMEPAD_HAT, 1, 0, CONSTANT));
        inputs[0].SetPinMode(INPUT_PULLUP);
        inputs[0].SetPin(27);
        inputs[0].SetIsAnalog(true);
        inputs[0].SetIsInverted(false);
        inputs[0].SetFilteringSamples(8);
        inputs[0].SetDeadZone(726);
        inputs[0].InitInput();

        inputs[1].SetBinding(Binding(GAMEPAD, GAMEPAD_HAT, 2, 0, CONSTANT));
        inputs[1].SetPinMode(INPUT);
        inputs[1].SetPin(35);
        inputs[1].SetIsAnalog(true);
        inputs[1].SetIsInverted(true);
        inputs[1].SetFilteringSamples(3);
        inputs[1].InitInput();

        inputs[2].SetBinding(Binding(GAMEPAD, GAMEPAD_HAT, 3, 0, CONSTANT));
        inputs[2].SetPinMode(INPUT);
        inputs[2].SetPin(39);
        inputs[2].SetIsAnalog(true);
        inputs[2].SetIsInverted(true);
        inputs[2].SetFilteringSamples(16);
        inputs[2].InitInput();

        // int testPins[] = {32, 33, 25, 26, 27, 14, 12, 13, 15, 2, 4, 16};

        // for (int i = 3; i < 3 + 12; i++)
        // {

        //     inputs[i].SetBinding(Binding(GAMEPAD, GAMEPAD_BUTTON, i - 2));
        //     inputs[i].SetPinMode(INPUT_PULLUP);
        //     inputs[i].SetPin(testPins[i - 3]);
        //     inputs[i].SetIsAnalog(false);
        //     inputs[i].SetIsInverted(true);
        //     inputs[i].InitInput();
        // }

        // int hatPins[] = {17, 18, 19, 21};

        // for (int i = 0; i < 4; i++)
        // {
        //     inputs[15 + i].SetBinding(Binding(GAMEPAD, GAMEPAD_HAT, 1 + i * 2, 0, CONSTANT));
        //     inputs[15 + i].SetPinMode(INPUT_PULLUP);
        //     inputs[15 + i].SetPin(hatPins[i]);
        //     inputs[15 + i].SetIsAnalog(false);
        //     inputs[15 + i].SetIsInverted(true);
        //     inputs[15 + i].InitInput();
        // }
    }
}

void InitHID()
{
    gamepad = HIDInterface();
    gamepad.InitHID();
}

uint8_t GetAssignedInputCount()
{
    for (int i = 0; i < INPUT_COUNT; i++)
    {
        if (inputs[i].GetPin() == 255)
        {
            return i;
        }
    }
    return 0;
}

uint8_t GetMacroCount()
{
    for (int i = 0; i < MACRO_COUNT; i++)
    {
        if (macros[i].GetBindingCount() == 0)
        {
            return i;
        }
    }
    return 0;
}

void UpdateInputs()
{
    for (int i = 0; i < INPUT_COUNT; i++)
    {
        inputs[i].UpdateState();
    }

    for (int i = 0; i < INPUT_COUNT; i++)
    {
        // Serial.print(i);
        // Serial.print("\t");
        // Serial.println(inputs[i].GetBinding().GetTrigger());
        if (inputs[i].GetBinding().GetTrigger() == 4)
        {
            // if (i == 1)
            // {
            //     Serial.println(inputs[i].GetBinding().GetDeviceType());
            // }
            SendInput(inputs[i].GetBinding());
        }
        else
        {
            switch (inputs[i].GetBinding().GetState())
            {
            case BUTTON_UP:

                break;
            case BUTTON_ON_UP:
                if (inputs[i].GetBinding().GetTrigger() == BUTTON_ON_UP)
                {
                    //Serial.println(i);
                    SendInput(inputs[i].GetBinding());
                }
                break;
            case BUTTON_DOWN:

                break;
            case BUTTON_ON_DOWN:
                if (inputs[i].GetBinding().GetTrigger() == BUTTON_ON_DOWN)
                {
                    SendInput(inputs[i].GetBinding());
                }
                break;
            }
        }
    }

    for (int i = 0; i < MACRO_COUNT; i++)
    {
        if (macros[i].IsValid())
        {
            if (macros[i].IsActive())
            {
                Binding b = macros[i].GetNextBinding();
                if (b.GetDeviceType() != NONE)
                {
                    SendInput(b);
                }
            }
        }
    }

    gamepad.SendReport();
}

void SendInput(Binding binding)
{
    // Serial.println("SendInput()");
    // Serial.println(binding.GetDeviceType());
    switch (binding.GetDeviceType())
    {
    case NONE:

        break;
    case GAMEPAD:
        // Temp fix which fits macros in under gamepad
        //(should be own device type but device types not implemented yet)
        if (binding.GetInputType() == 4)
        {
            macros[binding.GetAssignedInput()].StartMacro();
        }
        else
        {
            SendGamepadInput(binding);
        }
        break;
    case KEYBOARD:

        break;
    case MOUSE:

        break;
    case MACRO:
        // Serial.println("MACRO");
        if (binding.GetVal() == 1)
        {
            // Serial.println("START MACRO");
            macros[binding.GetAssignedInput()].StartMacro();
        }
        break;
    }
}

void SendGamepadInput(Binding binding)
{
    // Serial.println(binding.GetInputType());
    switch (binding.GetInputType())
    {
    case GAMEPAD_BUTTON:
        gamepad.SetGamepadButton(binding.GetAssignedInput(),
                                 binding.GetVal());
        break;
    case GAMEPAD_AXIS:
        gamepad.SetGamepadAxis(binding.GetAssignedInput(),
                               binding.GetVal());
        break;
    case GAMEPAD_HAT:
        if (binding.GetVal() == 1)
        {
            gamepad.SetGamepadHat(binding.GetAssignedInput());
        }
        break;
    case GAMEPAD_SPECIAL:
        gamepad.SetGamepadSpecial(binding.GetAssignedInput(),
                                  binding.GetVal());
        break;
    }
}

void ReportInputConfig(uint8_t index)
{
    if (index >= 0 && index < INPUT_COUNT)
    {
        inputs[index].ReportConfig();
    }
}

void AddMacro(char *data)
{
    if (data[2] >= 0 && data[2] <= MACRO_COUNT)
    {
        macros[data[2]].NewMacro(data);
    }
}

void SetMacroName(char *data)
{

    if (data[2] >= 0 && data[2] <= MACRO_COUNT)
    {
        macros[data[2]].SetMacroName(data);
    }
}

void ReportMacroConfig(uint8_t index)
{
    if (index >= 0 && index < MACRO_COUNT)
    {
        macros[index].ReportConfig();
    }
}

void ReportInputValues()
{
    for (int i = 0; i < GetAssignedInputCount(); i++)
    {
        inputs[i].ReportInputValues();
    }
}

void UpdateInputConfig(char *data)
{
    inputs[data[2]].UpdateInputConfig(data);
}

void DeleteInput(uint8_t idx)
{
    for (int i = idx; i < GetAssignedInputCount(); i++)
    {
        inputs[i] = inputs[i + 1];
    }
    inputs[GetAssignedInputCount()].SetToDefaults();
}

void DeleteMacro(uint8_t idx)
{
    for (int i = idx; i < GetMacroCount(); i++)
    {
        macros[i] = macros[i + 1];
    }
    macros[GetMacroCount()].SetToDefaults();
}

void DeleteMacroBinding(uint8_t macroIdx, uint8_t bindingIdx)
{
    macros[macroIdx].DeleteBinding(bindingIdx);
}

void UpdateMacroBindingConfig(char *data)
{
    macros[data[2]].SetBindingConfig(data);
}

void LoadConfigFromNVM()
{
    // Load deviceName from EEPROM
    for (int i = 0; i < 16; i++)
    {
        deviceName[i] = char(Read8BitValue(memoryLocationDeviceID + i));
    }

    // Load i2cAddress from EEPROM
    i2c_address = Read8BitValue(memoryLocationI2CAddress);
}

void LoadInputsFromNVM()
{
    uint8_t inputCount = Read8BitValue(memoryLocationInputCount);
    char data[32];
    for (int i = 0; i < 32; i++)
    {
        data[i] = 0;
    }
    for (int i = 0; i < inputCount; i++)
    {
        for (int b = 0; b < 23; b++)
        {
            data[b] = Read8BitValue(memoryLocationInputs + i * bytesPerInput + b);
            // Serial.print(b);
            // Serial.print(' ');
            // Serial.println(int(data[b]));
        }
        ////  Serial.println();

        inputs[i].UpdateInputConfig(data);
    }
}

void LoadMacrosFromNVM()
{
    uint8_t macroCount = Read8BitValue(memoryLocationMacroCount);
    int currentByte = memoryLocationMacros;
    for (int i = 0; i < macroCount; i++)
    {
        for (int n = 0; n < 16; n++)
        {
            macros[i].macroName[n] = Read8BitValue(currentByte + n);
        }
        currentByte += 16;
        // macros[i].AddBinding(Binding());

        uint8_t bindingCount = Read8BitValue(currentByte);
        Serial.print("Binding count: ");
        Serial.println(bindingCount);
        currentByte += 1;
        char data[11];
        for (uint8_t z = 0; z < bindingCount; z++)
        {
            data[2] = i;
            data[3] = z;
            for (uint8_t b = 4; b < 11; b++)
            {
                data[b] = Read8BitValue(currentByte);
                currentByte += 1;
            }
            macros[i].AddBinding(Binding());
            macros[i].SetBindingConfig(data);
        }
    }
}

void SaveAllToNVM()
{
    // Save deviceName to NVM
    for (int i = 0; i < 16; i++)
    {
        Store8BitValue(memoryLocationDeviceID + i, deviceName[i]);
    }

    // Save i2cAddress to NVM
    Store8BitValue(memoryLocationI2CAddress, i2c_address);

    Store8BitValue(memoryLocationInputCount, GetAssignedInputCount());
    Serial.write(GetAssignedInputCount());
    for (int i = 0; i < GetAssignedInputCount() + 1; i++)
    {
        Store8BitValue(memoryLocationInputs + i * bytesPerInput + 3, inputs[i].GetPin());
        Store8BitValue(memoryLocationInputs + i * bytesPerInput + 4, inputs[i].GetPinMode());
        Store8BitValue(memoryLocationInputs + i * bytesPerInput + 5, inputs[i].IsAnalog());
        Store8BitValue(memoryLocationInputs + i * bytesPerInput + 6, inputs[i].GetIsInverted());

        Store8BitValue(memoryLocationInputs + i * bytesPerInput + 7, highByte(inputs[i].GetMinVal()));
        Store8BitValue(memoryLocationInputs + i * bytesPerInput + 8, lowByte(inputs[i].GetMinVal()));
        Store8BitValue(memoryLocationInputs + i * bytesPerInput + 9, highByte(inputs[i].GetMidVal()));
        Store8BitValue(memoryLocationInputs + i * bytesPerInput + 10, lowByte(inputs[i].GetMidVal()));
        Store8BitValue(memoryLocationInputs + i * bytesPerInput + 11, highByte(inputs[i].GetMaxVal()));
        Store8BitValue(memoryLocationInputs + i * bytesPerInput + 12, lowByte(inputs[i].GetMaxVal()));

        Store8BitValue(memoryLocationInputs + i * bytesPerInput + 13, highByte(inputs[i].GetDeadZone()));
        Store8BitValue(memoryLocationInputs + i * bytesPerInput + 14, lowByte(inputs[i].GetDeadZone()));
        Store8BitValue(memoryLocationInputs + i * bytesPerInput + 15, inputs[i].GetBufferSize());

        Store8BitValue(memoryLocationInputs + i * bytesPerInput + 16, inputs[i].GetBinding().GetDeviceType());
        Store8BitValue(memoryLocationInputs + i * bytesPerInput + 17, inputs[i].GetBinding().GetInputType());
        Store8BitValue(memoryLocationInputs + i * bytesPerInput + 18, inputs[i].GetBinding().GetAssignedInput());

        Store8BitValue(memoryLocationInputs + i * bytesPerInput + 19, highByte(inputs[i].GetBinding().GetVal()));

        Store8BitValue(memoryLocationInputs + i * bytesPerInput + 20, lowByte(inputs[i].GetBinding().GetVal()));

        Store8BitValue(memoryLocationInputs + i * bytesPerInput + 22, inputs[i].GetBinding().GetTrigger());
    }

    Store8BitValue(memoryLocationMacroCount, GetMacroCount());
    Serial.write(123);
    Serial.write(GetMacroCount());
    int currentByte = memoryLocationMacros;
    for (int i = 0; i < GetMacroCount(); i++)
    {
        for (int n = 0; n < 16; n++)
        {
            Store8BitValue(currentByte + n, macros[i].macroName[n]);
        }
        currentByte += 16;

        uint8_t bindingCount = macros[i].GetBindingCount();
        Store8BitValue(currentByte, bindingCount);
        currentByte++;

        for (int b = 0; b < bindingCount; b++)
        {
            Store8BitValue(currentByte + 0, macros[i].GetBinding(b).GetDeviceType());
            Store8BitValue(currentByte + 1, macros[i].GetBinding(b).GetInputType());
            Store8BitValue(currentByte + 2, macros[i].GetBinding(b).GetAssignedInput());
            Store8BitValue(currentByte + 3, highByte(macros[i].GetBinding(b).GetVal()));
            Store8BitValue(currentByte + 4, lowByte(macros[i].GetBinding(b).GetVal()));
            Store8BitValue(currentByte + 5, macros[i].GetBinding(b).GetState());
            Store8BitValue(currentByte + 6, macros[i].GetBinding(b).GetTrigger());
            currentByte += 7;
        }
    }
}

void OnFirstRun()
{
    Serial.println("First Run");
    char defaultName[] = "NEW_DEVICE      ";
    for (int i = 0; i < 16; i++)
    {
        Store8BitValue(memoryLocationDeviceID + i, defaultName[i]);
    }

    // Write default i2c_address to EEPROM
    Store8BitValue(memoryLocationI2CAddress, 0);

    Store8BitValue(memoryLocationFirstRun, 1); // mark first run byte

    Store8BitValue(memoryLocationInputCount, 0);

    Store8BitValue(memoryLocationMacroCount, 0);
}
