#define COMMAND_SIZE 400

#define INCOMING_REQUEST 36 // '$'
#define RESPONSE_HEADER 37
#define INCOMING_HANDSHAKE_REQUEST 72 // 'H'
#define INCOMING_REQUEST_DEVICE_CONFIG 100
#define INCOMING_REQUEST_DEVICE_CONFIG_RESPONSE 101
#define INCOMING_REQUEST_INPUT_CONFIG 102
#define INCOMING_REQUEST_INPUT_CONFIG_RESPONSE 103
#define INCOMING_REQUEST_INPUT_ALL_CONFIG 104
#define INCOMING_REQUEST_INPUT_ALL_CONFIG_RESPONSE 105
#define INCOMING_RECEIVE_INPUT_CONFIG_UPDATE 106
#define INCOMING_RECEIVE_INPUT_CONFIG_UPDATE_RESPONSE 107
#define INCOMING_DELETE_INPUT_UPDATE 108
#define INCOMING_RESPONSE_DELETE_INPUT_UPDATE 109
#define INCOMING_REQUEST_INPUT_VALUES 110
#define INCOMING_RESPONSE_INPUT_VALUES 111
#define INCOMING_REQUEST_SAVE_TO_FLASH 112
#define INCOMING_RESPONSE_SAVE_TO_FLASH 113
#define INCOMING_REQUEST_MACRO_CONFIG 114
#define INCOMING_REQUEST_MACRO_CONFIG_RESPONSE 115
#define INCOMING_REQUEST_MACRO_BINDING_UPDATE 116
#define INCOMING_REQUEST_MACRO_BINDING_UPDATE_RESPONSE 117
#define INCOMING_REQUEST_MACRO_NAME_UPDATE 118
#define INCOMING_REQUEST_MACRO_NAME_UPDATE_RESPONSE 119
#define INCOMING_REQUEST_MACRO_CONFIG_UPDATE 120
#define INCOMING_REQUEST_MACRO_CONFIG_UPDATE_RESPONSE 121
#define INCOMING_REQUEST_DELETE_MACRO_UPDATE 122
#define INCOMING_REQUEST_DELETE_MACRO_UPDATE_RESPONSE 123
#define INCOMING_REQUEST_DELETE_MACRO_BINDING_UPDATE 124
#define INCOMING_REQUEST_DELETE_MACRO_BINDING_UPDATE_RESPONSE 125

bool connectedToPC = false;
static char data[COMMAND_SIZE];
static int serial_count;
static int c;

char b = 'E';


uint8_t zero = 0; // need this to insert 0 into Serial.write(0);

void InitComms()
{
    Serial.begin(115200);
    delay(400);
}

void CheckComms()
{
    while (Serial.available() > 0)
    {
        c = Serial.read();
        if (serial_count < COMMAND_SIZE)
        {
            data[serial_count] = c;
            serial_count++;
        }
    }

    // Incoming packet should be at least one byte plus the EOL \r\n
    if (serial_count > 2)
    {
        // Serial.println(serial_count);
        if (data[serial_count - 2] == '\r' && data[serial_count - 1] == '\n')
        {
            InterpretData();
            // Serial.println("Received");
        }
        ClearData();
    }
    else
    {
        ClearData();
    }
}

void InterpretData()
{
    if (data[0] == '\r' || data[0] == '\n')
    {
        return;
    }

    // if (data[0] == 'H' && data[1] == 'E')
    // {
    // }

    else if (data[0] == INCOMING_REQUEST)
    {
        switch (data[1])
        {
        case INCOMING_HANDSHAKE_REQUEST:
            Serial.write(RESPONSE_HEADER);
            Serial.write(INCOMING_HANDSHAKE_REQUEST);
            // Serial.println("Hello yourself!");
            Serial.write('\r');
            Serial.write('\n');
            break;
        case INCOMING_REQUEST_INPUT_VALUES:
            Serial.write(RESPONSE_HEADER);
            Serial.write(INCOMING_RESPONSE_INPUT_VALUES);
            Serial.write(GetAssignedInputCount());
            ReportInputValues();
            Serial.write('\r');
            Serial.write('\n');
            break;
        case INCOMING_REQUEST_DEVICE_CONFIG:
            Serial.write(RESPONSE_HEADER);
            Serial.write(INCOMING_REQUEST_DEVICE_CONFIG_RESPONSE);
            Serial.write((uint8_t)DEVICE_TYPE);
            Serial.write((uint8_t)FIRMWARE_VERSION);
            Serial.write(GetAssignedInputCount());
            Serial.write(GetMacroCount());
            Serial.write(zero); // spare
            Serial.write(zero); // spare
            Serial.write(zero); // spare
            Serial.write('\r');
            Serial.write('\n');
            break;
        case INCOMING_REQUEST_INPUT_CONFIG:
            Serial.write(RESPONSE_HEADER);
            // Byte #2 contains requested input index
            // Serial.print(data[2]);
            Serial.write(INCOMING_REQUEST_INPUT_CONFIG_RESPONSE);
            Serial.write(data[2]);
            ReportInputConfig(data[2]);
            Serial.write('\r');
            Serial.write('\n');
            break;
        case INCOMING_REQUEST_MACRO_CONFIG:
            Serial.write(RESPONSE_HEADER);
            Serial.write(INCOMING_REQUEST_MACRO_CONFIG_RESPONSE);
            Serial.write(data[2]);
            ReportMacroConfig(data[2]);
            Serial.write('\r');
            Serial.write('\n');
            break;
        case INCOMING_REQUEST_INPUT_ALL_CONFIG:
            Serial.write(RESPONSE_HEADER);
            // Byte #2 contains requested input index
            // Serial.print(data[2]);
            Serial.write(INCOMING_REQUEST_INPUT_ALL_CONFIG_RESPONSE);
            // Serial.write(0);
            Serial.write(GetAssignedInputCount());
            for (int i = 0; i < GetAssignedInputCount(); i++)
            {
                ReportInputConfig(i);
            }
            Serial.write('\r');
            Serial.write('\n');
            break;
        case INCOMING_RECEIVE_INPUT_CONFIG_UPDATE:
            Serial.write(RESPONSE_HEADER);
            // data[2] == input index

            UpdateInputConfig(data);
            Serial.write(INCOMING_RECEIVE_INPUT_CONFIG_UPDATE_RESPONSE);

            Serial.write(data[2]);
            Serial.write('o');
            Serial.write('k');

            Serial.write('\r');
            Serial.write('\n');

            break;
        case INCOMING_DELETE_INPUT_UPDATE:
            Serial.write(RESPONSE_HEADER);
            DeleteInput(data[2]);
            Serial.write(INCOMING_RESPONSE_DELETE_INPUT_UPDATE);

            Serial.write(data[2]);
            Serial.write('o');
            Serial.write('k');

            Serial.write('\r');
            Serial.write('\n');
            break;

        
        case INCOMING_REQUEST_DELETE_MACRO_UPDATE:
            Serial.write(RESPONSE_HEADER);
            DeleteMacro(data[2]);
            Serial.write(INCOMING_REQUEST_DELETE_MACRO_UPDATE_RESPONSE);

            Serial.write(data[2]);
            Serial.write('o');
            Serial.write('k');

            Serial.write('\r');
            Serial.write('\n');
            break;

        case INCOMING_REQUEST_DELETE_MACRO_BINDING_UPDATE:
            Serial.write(RESPONSE_HEADER);
            DeleteMacroBinding(data[2], data[3]);
            Serial.write(INCOMING_REQUEST_DELETE_MACRO_BINDING_UPDATE_RESPONSE);

            Serial.write(data[2]);
            Serial.write(data[3]);
            Serial.write('o');
            Serial.write('k');

            Serial.write('\r');
            Serial.write('\n');
            break;

        case INCOMING_REQUEST_MACRO_CONFIG_UPDATE:
            Serial.write(RESPONSE_HEADER);
            Serial.write(INCOMING_REQUEST_MACRO_CONFIG_UPDATE_RESPONSE);
            Serial.write(data[2]);
            AddMacro(data);
            Serial.write('o');
            Serial.write('k');
            Serial.write('\r');
            Serial.write('\n');

            break;

        case INCOMING_REQUEST_MACRO_NAME_UPDATE:
            Serial.write(RESPONSE_HEADER);
            Serial.write(INCOMING_REQUEST_MACRO_NAME_UPDATE_RESPONSE);
            SetMacroName(data);
            Serial.write('o');
            Serial.write('k');
            Serial.write('\r');
            Serial.write('\n');
            break;
        case INCOMING_REQUEST_MACRO_BINDING_UPDATE:

            UpdateMacroBindingConfig(data);

            Serial.write(RESPONSE_HEADER);
            Serial.write(INCOMING_REQUEST_MACRO_BINDING_UPDATE_RESPONSE);
            Serial.write(data[2]);
            Serial.write('o');
            Serial.write('k');
            Serial.write('\r');
            Serial.write('\n');
            break;
        case INCOMING_REQUEST_SAVE_TO_FLASH:
            Serial.write(RESPONSE_HEADER);
            Serial.write(INCOMING_RESPONSE_SAVE_TO_FLASH);
            SaveAllToNVM();
            Serial.write('o');
            Serial.write('k');

            Serial.write('\r');
            Serial.write('\n');

            break;
        default:
            Serial.write(RESPONSE_HEADER);
            Serial.println("UNKNOWN MESSAGE");
        }
    }
}

void ClearData()
{
    for (int i = 0; i < COMMAND_SIZE; i++)
    {
        data[i] = 0;
    }
    serial_count = 0;
}
