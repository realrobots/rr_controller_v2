#include <Joystick.h>

#define numOfButtons 128
#define numOfHatSwitches 4

#define enableX false
#define enableY false
#define enableZ true
#define enableRX true
#define enableRY true
#define enableRZ false
#define enableSlider1 true
#define enableSlider2 false

#define enableSteering true    // X
#define enableAccelerator true // Y
#define enableRudder false     // RZ / RUDDER
#define enableBrake true       // RZ / RUDDER
#define enableThrottle true    // SLIDER 2

int8_t hat;
uint32_t buttons;

class HIDInterface
{

private:
    // BleGamepad bleGamepad;
    // BleGamepadConfiguration bleGamepadConfig;
    // USBHIDGamepad Gamepad;

    // std::string btName = "RealRobots HID Device";
    // std::string btManufacturer = "RealRobots";

    uint8_t hats[4] = {};

public:
    void InitHID()
    {
        Serial.println("Starting USBHID work!");
        // bleGamepad = BleGamepad(btName, btManufacturer, 100);
        // bleGamepadConfig.setAutoReport(false);
        // bleGamepadConfig.setControllerType(CONTROLLER_TYPE_GAMEPAD); // CONTROLLER_TYPE_JOYSTICK, CONTROLLER_TYPE_GAMEPAD (DEFAULT), CONTROLLER_TYPE_MULTI_AXIS
        // bleGamepadConfig.setButtonCount(numOfButtons);
        // bleGamepadConfig.setHatSwitchCount(numOfHatSwitches);
        // bleGamepadConfig.setWhichSpecialButtons(true, true, true, true, true, true, true, true);
        // bleGamepadConfig.setVid(0xe502);
        // bleGamepadConfig.setPid(0xabcd);
        // bleGamepadConfig.setAxesMin(0x0000); // 0 --> int16_t - 16 bit signed integer - Can be in decimal or hexadecimal
        // bleGamepadConfig.setAxesMax(0x7FFF); // 32767 --> int16_t - 16 bit signed integer - Can be in decimal or hexadecimal
        // bleGamepadConfig.setWhichAxes(enableX, enableY, enableZ, enableRX, enableRY, enableRZ, enableSlider1, enableSlider2);
        // bleGamepadConfig.setWhichSimulationControls(enableRudder, enableThrottle, enableAccelerator, enableBrake, enableSteering);

        // bleGamepad.begin(&bleGamepadConfig);
        Joystick.begin();
        Joystick.useManualSend(true);
        //USB.begin();
    }

    void SetGamepadButton(uint8_t button, uint8_t state)
    {
        Joystick.button(button, state);        
    }

    // Special buttons not working, not sure why
    void SetGamepadSpecial(uint8_t button, uint8_t state)
    {
        if (state)
        {

            // Gamepad.pressSpecialButton(button);
        }
        else
        {
            // Gamepad.pressSpecialButton(button);
        }
    }

    // Hat number = hatIdx/9, hat position = hatIdx % 9
    void SetGamepadHat(uint8_t hatIdx)
    {
       

        hat = hatIdx;
        
    }

    void SetGamepadAxis(uint8_t axis, int16_t value)
    {
        // Serial.print(axis);
        // Serial.print("\t");
        // Serial.println(value);
        value = map(value, 0, 32767, 0, 1023);

        switch (axis)
        {
        case 0: // X Axis
            Joystick.X(value);
            break;
        case 1: // Y Axis
            Joystick.Y(value);
            break;
        case 2: // Z Axis
            Joystick.Z(value);
            break;
        case 3: // rX Axis
            //Joystick.Xrotate(value);
            break;
        case 4: // rY Axis
            //Joystick.Yrotate(value);
            break;
        case 5: // rZ Axis
            Joystick.Zrotate(value);
            break;
        case 6: // Slider1 Axis
            Joystick.sliderLeft(value);
            break;
        case 7: // Slider2 Axis
            Joystick.sliderRight(value);
            break;
        case 8:
            // Gamepad.setSteering(value);
            break;
        case 9:
            // Gamepad.setAccelerator(value);
            break;
        case 10:
            // Gamepad.setRudder(value);
            break;
        case 11:
            // Gamepad.setBrake(value);
            break;
        case 12:
            // Gamepad.setThrottle(value);
            break;
        case 13:
            // Gamepad.setBatteryLevel(map(value, 0, 32767, 0, 100));
            break;
        }
    }

    void SendHats()
    {
        if (hat == -1){
            Joystick.hat(-1);
        } else {
            Joystick.hat(hat * 45);
        }
        
        //Serial.println(hats[0]*45);
        hat = -1;
        
    }

    void SendReport()
    {
        // for (int i = 0; i < 32; i++){
        //     Joystick.button(i+1, bitRead(buttons, i));
        // }
        SendHats();

        Joystick.send_now();
        //Gamepad.send(x, y, z, rz, rx, ry, hats[0], buttons);
    }
};